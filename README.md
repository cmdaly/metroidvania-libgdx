To access a live demo, visit: [http://bojjenclon.com/projects/metroidvania/](http://bojjenclon.com/projects/metroidvania/)  
Note: Demo may be slightly out of date.

Controls (Keyboard):

* Arrows/WASD = Movement
* E = Attack
* Q = Dash

Controls (Xbox Controller):

* Left Stick = Movement
* A Button = Attack
* Triggers = Dash