package com.bojjenclon.metroidvania.client;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;
import com.bojjenclon.metroidvania.Config;
import com.bojjenclon.metroidvania.Metroidvania;

public class HtmlLauncher extends GwtApplication {

        @Override
        public GwtApplicationConfiguration getConfig () {
                return new GwtApplicationConfiguration(Config.WIDTH, Config.HEIGHT);
        }

        @Override
        public ApplicationListener getApplicationListener () {
                return new Metroidvania();
        }
}