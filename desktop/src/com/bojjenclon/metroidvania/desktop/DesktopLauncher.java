package com.bojjenclon.metroidvania.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.bojjenclon.metroidvania.Config;
import com.bojjenclon.metroidvania.Metroidvania;

public class DesktopLauncher
{
    public static void main(String[] arg)
    {
        LwjglApplicationConfiguration l_config = new LwjglApplicationConfiguration();

        l_config.title = "Metroidvania";
        l_config.width = Config.WIDTH;
        l_config.height = Config.HEIGHT;

        new LwjglApplication(new Metroidvania(), l_config);
    }
}
