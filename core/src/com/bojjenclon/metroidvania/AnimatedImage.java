package com.bojjenclon.metroidvania;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.util.HashMap;

/**
 * Created by Cornell on 12/31/2014.
 */
public class AnimatedImage
{
    private Texture m_texture;
    private TextureRegion[] m_textureRegions;
    private HashMap<String, Animation> m_animations;
    private Animation m_curAnimation;
    private String m_animationName;
    private TextureRegion m_curFrame;
    private float m_stateTime;
    private boolean m_finished;

    public AnimatedImage(Texture p_texture, int p_frameCols, int p_fromRows)
    {
        m_texture = p_texture;
        m_textureRegions = new TextureRegion[p_frameCols * p_fromRows];
        m_animations = new HashMap<String, Animation>();
        m_curAnimation = null;
        m_animationName = null;
        m_curFrame = null;
        m_stateTime = 0;
        m_finished = false;

        TextureRegion[][] l_tempRegions = TextureRegion.split(m_texture, m_texture.getWidth() / p_frameCols, m_texture.getHeight() / p_fromRows);

        int index = 0;
        for (int i = 0; i < l_tempRegions.length; i++)
        {
            for (int j = 0; j < l_tempRegions[i].length; j++)
            {
                m_textureRegions[index] = l_tempRegions[i][j];

                index++;
            }
        }
    }

    public void addAnimation(String p_name, int[] p_frames, float p_frameDuration, boolean p_loop)
    {
        TextureRegion[] l_textureRegions = new TextureRegion[p_frames.length];

        for (int i = 0; i < p_frames.length; i++)
        {
            l_textureRegions[i] = m_textureRegions[p_frames[i]];
        }

        Animation l_animation = new Animation(p_frameDuration, l_textureRegions);
        if (p_loop)
        {
            l_animation.setPlayMode(Animation.PlayMode.LOOP);
        }

        m_animations.put(p_name, l_animation);
    }

    public Animation getAnimation(String p_name)
    {
        return m_animations.get(p_name);
    }

    public Animation getCurAnimation()
    {
        return m_curAnimation;
    }

    public TextureRegion getCurFrame()
    {
        return m_curFrame;
    }

    public String getAnimationName()
    {
        return m_animationName;
    }

    public void play(String p_name)
    {
        m_curAnimation = m_animations.get(p_name);
        m_animationName = p_name;

        m_stateTime = 0;
        m_finished = false;
    }

    public boolean isFinished()
    {
        return m_finished;
    }

    public void update(float p_dt)
    {
        if (m_finished)
        {
            return;
        }

        m_stateTime += p_dt;

        m_curFrame = m_curAnimation.getKeyFrame(m_stateTime);

        if (m_curAnimation.isAnimationFinished(m_stateTime))
        {
            if (m_curAnimation.getPlayMode() == Animation.PlayMode.LOOP)
            {
                m_stateTime = 0;
            }
            else
            {
                /*m_curAnimation = null;
                m_animationName = null;
                m_curFrame = null;*/

                m_finished = true;
            }
        }
    }
}
