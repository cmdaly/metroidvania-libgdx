package com.bojjenclon.metroidvania;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.bojjenclon.metroidvania.json.WeaponEntry;

import java.util.HashMap;

/**
 * Created by Cornell on 1/8/2015.
 */
public abstract class Global
{
    public static final AssetManager assets = new AssetManager();
    public static final SpriteBatch batch = new SpriteBatch();
    public static final BitmapFont font = new BitmapFont();

    public static final HashMap<String, WeaponEntry> weapons = new HashMap<String, WeaponEntry>();
}
