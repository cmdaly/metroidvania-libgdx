package com.bojjenclon.metroidvania.screens;

import com.badlogic.ashley.core.Engine;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.bojjenclon.metroidvania.Config;
import com.bojjenclon.metroidvania.EntityManager;
import com.bojjenclon.metroidvania.Global;
import com.bojjenclon.metroidvania.Metroidvania;
import com.bojjenclon.metroidvania.collision.grid.GridWorld;
import com.bojjenclon.metroidvania.systems.*;
import com.bojjenclon.metroidvania.ui.HUD;

/**
 * Created by Cornell on 12/24/2014.
 */
public class GameScreen implements Screen
{
    private final Metroidvania m_game;
    private OrthographicCamera m_camera;
    private Viewport m_viewport;

    /* Ashley (Component/Entity) */
    private Engine m_engine;

    /* Scene2d (for UI) */
    private HUD m_hud;

    private GridWorld m_grid;

    private FPSLogger m_fpsLogger;

    public GameScreen(final Metroidvania p_game)
    {
        m_game = p_game;

        m_camera = new OrthographicCamera();
        m_camera.setToOrtho(false, Config.WIDTH, Config.HEIGHT);

        m_viewport = new FitViewport(Config.WIDTH, Config.HEIGHT);

        m_engine = new Engine();

        m_hud = new HUD();

        m_grid = new GridWorld(Config.WIDTH, Config.HEIGHT, 50, 50);

        EntityManager.setGame(m_game);
        EntityManager.setEngine(m_engine);
        EntityManager.setGrid(m_grid);

        m_engine.addSystem(new HitDelaySystem());
        m_engine.addSystem(new VelocitySmoothingSystem());
        m_engine.addSystem(new InputSystem(m_game));
        m_engine.addSystem(new DashSystem());
        m_engine.addSystem(new GravitySystem());
        m_engine.addSystem(new MovementSystem());
        m_engine.addSystem(new CollisionSystem(m_grid));
        m_engine.addSystem(new AnchorSystem());
        m_engine.addSystem(new WeaponSystem());
        m_engine.addSystem(new AnimationSystem());
        m_engine.addSystem(new StatusSystem());
        m_engine.addSystem(new RenderSystem(Global.batch));
        m_engine.addSystem(new HUDSystem(m_hud));
        //m_engine.addSystem(new CollisionDebugRenderSystem());

        m_engine.addEntity(EntityManager.createPlayer(60, 200));
        m_engine.addEntity(EntityManager.createCannibal(200, 160));

        m_engine.addEntity(EntityManager.createWall(16, 62));
        m_engine.addEntity(EntityManager.createWall(16, 94));
        m_engine.addEntity(EntityManager.createWall(16, 126));
        for (int i = 0; i < 15; i++)
        {
            m_engine.addEntity(EntityManager.createFloor(48 + (32 * i), 30));
        }
        m_engine.addEntity(EntityManager.createWall(32 * 16, 62));
        m_engine.addEntity(EntityManager.createWall(32 * 16, 94));
        m_engine.addEntity(EntityManager.createWall(32 * 16, 126));

        m_fpsLogger = new FPSLogger();
    }

    @Override
    public void render(float p_delta)
    {
        m_engine.update(p_delta);
        m_hud.render(p_delta);

        m_fpsLogger.log();
    }

    @Override
    public void resize(int p_width, int p_height)
    {
        m_viewport.update(p_width, p_height);

        m_hud.resize(p_width, p_height);
    }

    @Override
    public void pause()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void show()
    {

    }

    @Override
    public void hide()
    {

    }

    @Override
    public void dispose()
    {
        m_hud.dispose();
    }
}
