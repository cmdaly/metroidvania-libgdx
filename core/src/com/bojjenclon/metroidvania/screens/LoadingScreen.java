package com.bojjenclon.metroidvania.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.async.AsyncExecutor;
import com.badlogic.gdx.utils.async.AsyncTask;
import com.bojjenclon.metroidvania.Config;
import com.bojjenclon.metroidvania.Global;
import com.bojjenclon.metroidvania.Metroidvania;
import com.bojjenclon.metroidvania.json.WeaponData;
import com.bojjenclon.metroidvania.json.WeaponEntry;
import com.bojjenclon.metroidvania.loaders.WeaponDataLoader;

/**
 * Created by Cornell on 12/24/2014.
 */
public class LoadingScreen implements Screen
{
    private final Metroidvania m_game;
    private OrthographicCamera m_camera;

    private AsyncExecutor m_asyncExecutor;

    private boolean m_weaponsConfigured;

    public LoadingScreen(final Metroidvania p_game)
    {
        m_game = p_game;

        m_camera = new OrthographicCamera();
        m_camera.setToOrtho(false, Config.WIDTH, Config.HEIGHT);

        m_asyncExecutor = new AsyncExecutor(4);

        m_weaponsConfigured = false;

        prepareAssets();
    }

    @Override
    public void render(float p_delta)
    {
        if (Global.assets.update())
        {
            if (!m_weaponsConfigured)
            {
                configureWeapons();
            }
            else
            {
                m_game.setScreen(new GameScreen(m_game));
                dispose();
            }
        }

        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        m_camera.update();
        Global.batch.setProjectionMatrix(m_camera.combined);

        Global.batch.begin();
        Global.font.draw(Global.batch, "Loading: " + Global.assets.getProgress(), 100, 150);
        Global.batch.end();
    }

    @Override
    public void resize(int p_width, int p_height)
    {

    }

    @Override
    public void pause()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void show()
    {

    }

    @Override
    public void hide()
    {

    }

    @Override
    public void dispose()
    {

    }

    private void prepareAssets()
    {
        Global.assets.setLoader(WeaponData.class, new WeaponDataLoader(new InternalFileHandleResolver()));

        Global.assets.load("gfx/ui.txt", TextureAtlas.class);

        Global.assets.load("gfx/battlemage.gif", Texture.class);
        Global.assets.load("gfx/cannibal.gif", Texture.class);
        Global.assets.load("gfx/crate.gif", Texture.class);
        Global.assets.load("gfx/bigCrate.png", Texture.class);
        Global.assets.load("gfx/longSword.png", Texture.class);
        Global.assets.load("gfx/roomFloor31.gif", Texture.class);
        Global.assets.load("gfx/wall13.gif", Texture.class);

        Global.assets.load("data/weapons.json", WeaponData.class);
    }

    private void configureWeapons()
    {
        if (Global.assets.isLoaded("data/weapons.json", WeaponData.class))
        {
            m_asyncExecutor.submit(new AsyncTask<Boolean>()
            {
                @Override
                public Boolean call()
                {
                    WeaponData l_weaponData = Global.assets.get("data/weapons.json", WeaponData.class);

                    for (WeaponEntry l_entry : l_weaponData.weapons)
                    {
                        Global.weapons.put(l_entry.name, l_entry);
                    }

                    m_weaponsConfigured = true;

                    return m_weaponsConfigured;
                }
            });
        }
    }
}
