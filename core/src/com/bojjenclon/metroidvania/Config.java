package com.bojjenclon.metroidvania;

/**
 * Created by Cornell on 12/24/2014.
 */
public abstract class Config
{
    public static final int WIDTH = 800;
    public static final int HEIGHT = 600;

    public static final float GRAVITY = 50.0f;
    public static final float MAX_FALL_SPEED = -360.0f;
    public static final float MAX_JUMP_SPEED = 260.0f;

    // move to input storage class later (with config options for player)
    public static float leftAxisDeadZone = 0.5f;
    public static float leftTriggerDeadZone = 0.25f;
    public static float rightTriggerDeadZone = 0.25f;
}
