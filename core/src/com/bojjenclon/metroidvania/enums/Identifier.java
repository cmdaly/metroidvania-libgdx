package com.bojjenclon.metroidvania.enums;

/**
 * Created by Cornell on 12/25/2014.
 */
public enum Identifier
{
    Floor,
    Wall,
    Player,
    Enemy,
    NPC,
    Weapon
}

