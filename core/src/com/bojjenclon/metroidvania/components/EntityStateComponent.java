package com.bojjenclon.metroidvania.components;

import com.badlogic.ashley.core.Component;

/**
 * Created by Cornell on 12/30/2014.
 */
public class EntityStateComponent extends Component
{
    public boolean enabled = true;
}
