package com.bojjenclon.metroidvania.components;

import com.badlogic.ashley.core.Component;
import com.bojjenclon.metroidvania.enums.Direction;

/**
 * Created by Cornell on 1/4/2015.
 */
public class DirectionComponent extends Component
{
    public Direction direction = Direction.Right;
}
