package com.bojjenclon.metroidvania.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.bojjenclon.metroidvania.json.WeaponData;

/**
 * Created by Cornell on 12/30/2014.
 */
public class EquipmentComponent extends Component
{
    public WeaponData weaponData;
    public Entity weaponEntity;
}
