package com.bojjenclon.metroidvania.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by Cornell on 12/24/2014.
 */
public class TransformComponent extends Component
{
    public final Vector3 pos = new Vector3();
    public final Vector2 origin = new Vector2();
    public final Vector2 scale = new Vector2(1.0f, 1.0f);
    public float rotation = 0.0f;
    public Vector2 size = new Vector2(1.0f, 1.0f);
}
