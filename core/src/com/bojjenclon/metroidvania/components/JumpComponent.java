package com.bojjenclon.metroidvania.components;

import com.badlogic.ashley.core.Component;

/**
 * Created by Cornell on 12/25/2014.
 */
public class JumpComponent extends Component
{
    public float jumpDistance = 0;
    public boolean canJump = true;
}
