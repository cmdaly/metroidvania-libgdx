package com.bojjenclon.metroidvania.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Cornell on 12/26/2014.
 */
public class VelocityComponent extends Component
{
    public Vector2 velocity = new Vector2();
    public Vector2 oldVelocity = new Vector2();
}
