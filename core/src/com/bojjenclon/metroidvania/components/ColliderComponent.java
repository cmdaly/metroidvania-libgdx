package com.bojjenclon.metroidvania.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.utils.Array;
import com.bojjenclon.metroidvania.collision.shapes.Shape;

/**
 * Created by Cornell on 12/26/2014.
 */
public class ColliderComponent extends Component
{
    public Shape shape;
    //public boolean collisionOccurred = false;
    public Array<Entity> collidedWith = new Array<Entity>();
    public boolean isStatic = false;

    public boolean touchingFloor = false;
}
