package com.bojjenclon.metroidvania.components;

import com.badlogic.ashley.core.Component;
import com.bojjenclon.metroidvania.AnimatedImage;

/**
 * Created by Cornell on 12/31/2014.
 */
public class AnimationComponent extends Component
{
    public AnimatedImage animatedImage;
}
