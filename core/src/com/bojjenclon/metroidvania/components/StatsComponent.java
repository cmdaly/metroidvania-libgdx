package com.bojjenclon.metroidvania.components;

import com.badlogic.ashley.core.Component;

/**
 * Created by Cornell on 1/1/2015.
 */
public class StatsComponent extends Component
{
    public int hp = 10;
    public int maxHP = 10;

    public int mp = 5;
    public int maxMP = 5;
}
