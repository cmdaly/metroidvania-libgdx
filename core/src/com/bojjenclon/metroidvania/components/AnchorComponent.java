package com.bojjenclon.metroidvania.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by Cornell on 12/30/2014.
 */
public class AnchorComponent extends Component
{
    public Entity parent = null;
    public Vector3 relativePosition = new Vector3();
}
