package com.bojjenclon.metroidvania.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.Texture;

/**
 * Created by Cornell on 12/24/2014.
 */
public class TextureComponent extends Component
{
    public Texture texture;
}
