package com.bojjenclon.metroidvania.components;

import com.badlogic.ashley.core.Component;

/**
 * Created by Cornell on 1/2/2015.
 */
public class DashComponent extends Component
{
    public float dashDistance = 0;
    public boolean isDashing = false;
    public float dashDelay = -1;
    public boolean canDash = true;
    public boolean justDashed = false;
}
