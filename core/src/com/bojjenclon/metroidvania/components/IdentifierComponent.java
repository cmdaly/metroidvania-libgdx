package com.bojjenclon.metroidvania.components;

import com.badlogic.ashley.core.Component;
import com.bojjenclon.metroidvania.enums.Identifier;

/**
 * Created by Cornell on 12/25/2014.
 */
public class IdentifierComponent extends Component
{
    public Identifier type;
}
