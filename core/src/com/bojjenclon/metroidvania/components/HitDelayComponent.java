package com.bojjenclon.metroidvania.components;

import com.badlogic.ashley.core.Component;

/**
 * Created by Cornell on 1/1/2015.
 */
public class HitDelayComponent extends Component
{
    public float timer = 0;
    public boolean canBeHit = true;
    public boolean landedFromHit = true;
}
