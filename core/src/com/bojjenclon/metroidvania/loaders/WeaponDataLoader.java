package com.bojjenclon.metroidvania.loaders;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.AsynchronousAssetLoader;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonWriter;
import com.bojjenclon.metroidvania.json.WeaponData;

/**
 * Created by Cornell on 12/30/2014.
 */
public class WeaponDataLoader extends AsynchronousAssetLoader<WeaponData, WeaponDataLoader.WeaponDataParameter>
{
    public WeaponDataLoader(FileHandleResolver p_resolver)
    {
        super(p_resolver);
    }

    private WeaponData m_weaponData;

    @Override
    public void loadAsync(AssetManager p_manager, String p_fileName, FileHandle p_file, WeaponDataParameter p_parameter)
    {
        m_weaponData = null;

        Json l_json = new Json();
        l_json.setTypeName(null);
        l_json.setUsePrototypes(false);
        l_json.setIgnoreUnknownFields(true);
        l_json.setOutputType(JsonWriter.OutputType.json);

        m_weaponData = l_json.fromJson(WeaponData.class, p_file);
    }

    @Override
    public WeaponData loadSync(AssetManager p_manager, String p_fileName, FileHandle p_file, WeaponDataParameter p_parameter)
    {
        WeaponData l_weaponData = m_weaponData;
        m_weaponData = null;
        return l_weaponData;
    }

    @Override
    public Array<AssetDescriptor> getDependencies(String p_fileName, FileHandle p_file, WeaponDataParameter p_parameter)
    {
        return null;
    }

    static public class WeaponDataParameter extends AssetLoaderParameters<WeaponData>
    {
    }
}
