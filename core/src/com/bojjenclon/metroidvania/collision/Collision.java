package com.bojjenclon.metroidvania.collision;

import com.bojjenclon.metroidvania.collision.data.CollisionData;
import com.bojjenclon.metroidvania.collision.data.RayData;
import com.bojjenclon.metroidvania.collision.data.RayIntersectionData;
import com.bojjenclon.metroidvania.collision.math.Vector;
import com.bojjenclon.metroidvania.collision.shapes.Polygon;
import com.bojjenclon.metroidvania.collision.shapes.Ray;
import com.bojjenclon.metroidvania.collision.shapes.Shape;

import java.util.ArrayList;

/**
 * Created by Cornell on 12/26/2014.
 */
public abstract class Collision
{
    public static CollisionData test(Shape p_shape1, Shape p_shape2)
    {
        return p_shape1.test(p_shape2);
    }

    public static CollisionData[] testShapes(Shape p_shape, Shape[] p_shapes)
    {
        ArrayList<CollisionData> l_results = new ArrayList<CollisionData>();

        for (Shape l_otherShape : p_shapes)
        {
            CollisionData l_result = test(p_shape, l_otherShape);

            if (l_result != null)
            {
                l_results.add(l_result);
            }
        }

        return (CollisionData[]) l_results.toArray();
    }

    public static RayData rayShape(Ray p_ray, Shape p_shape)
    {
        return p_shape.testRay(p_ray);
    }

    public static RayData[] rayShapes(Ray p_ray, Shape[] p_shapes)
    {
        ArrayList<RayData> l_results = new ArrayList<RayData>();

        for (Shape l_shape : p_shapes)
        {
            RayData l_result = l_shape.testRay(p_ray);

            if (l_result != null)
            {
                l_results.add(l_result);
            }
        }

        return (RayData[]) l_results.toArray();
    }

    public static RayIntersectionData rayRay(Ray p_ray1, Ray p_ray2)
    {
        return Collision2D.rayRay(p_ray1, p_ray2);
    }

    public static RayIntersectionData[] rayShapes(Ray p_ray, Ray[] p_rays)
    {
        ArrayList<RayIntersectionData> l_results = new ArrayList<RayIntersectionData>();

        for (Ray l_otherRay : p_rays)
        {
            RayIntersectionData l_result = Collision2D.rayRay(p_ray, l_otherRay);

            if (l_result != null)
            {
                l_results.add(l_result);
            }
        }

        return (RayIntersectionData[]) l_results.toArray();
    }

    public static boolean pointInPoly(Vector p_point, Polygon p_polygon)
    {
        int l_sides = p_polygon.getNumSides();
        int j = (l_sides - 1);
        boolean l_oddNodes = false;

        for (int i = 0; i < l_sides; i++)
        {
            Vector[] l_transformedVertices = p_polygon.getTransformedVertices();

            if ((l_transformedVertices[i].y < p_point.y && l_transformedVertices[j].y >= p_point.y) ||
                (l_transformedVertices[j].y < p_point.y && l_transformedVertices[i].y >= p_point.y))
            {
                if (l_transformedVertices[i].x +
                    (p_point.y - l_transformedVertices[i].y) /
                    (l_transformedVertices[j].y - l_transformedVertices[i].y) *
                    (l_transformedVertices[j].x - l_transformedVertices[i].x) < p_point.x)
                {
                    l_oddNodes = !l_oddNodes;
                }
            }

            j = i;
        }

        return l_oddNodes;
    }
}
