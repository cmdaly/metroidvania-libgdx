package com.bojjenclon.metroidvania.collision.shapes;

import com.bojjenclon.metroidvania.collision.math.Vector;

/**
 * Created by Cornell on 12/26/2014.
 */
public class Ray
{
    public Vector start;
    public Vector end;

    public boolean isInfinite;

    public Ray(Vector p_start, Vector p_end)
    {
        this(p_start, p_end, true);
    }

    public Ray(Vector p_start, Vector p_end, boolean p_isInfinite)
    {
        start = p_start;
        end = p_end;

        isInfinite = p_isInfinite;
    }
}
