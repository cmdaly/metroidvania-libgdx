package com.bojjenclon.metroidvania.collision.shapes;

import com.bojjenclon.metroidvania.collision.Collision2D;
import com.bojjenclon.metroidvania.collision.data.CollisionData;
import com.bojjenclon.metroidvania.collision.data.RayData;

/**
 * Created by Cornell on 12/26/2014.
 */
public class Circle extends Shape
{
    private float m_radius;

    public Circle(float p_x, float p_y, float p_radius)
    {
        super(p_x, p_y);

        m_radius = p_radius;

        name = ("circle " + m_radius);
    }

    @Override
    public CollisionData test(Shape p_shape)
    {
        return p_shape.testCircle(this, true);
    }

    @Override
    public CollisionData testCircle(Circle p_circle, boolean p_flip)
    {
        Circle l_circle1 = (p_flip ? p_circle : this);
        Circle l_circle2 = (p_flip ? this : p_circle);

        return Collision2D.testCircles(l_circle1, l_circle2);
    }

    @Override
    public CollisionData testPolygon(Polygon p_polygon, boolean p_flip)
    {
        return Collision2D.testCircleVsPolygon(this, p_polygon, p_flip);
    }

    @Override
    public RayData testRay(Ray p_ray)
    {
        return Collision2D.rayCircle(p_ray, this);
    }

    public float getRadius()
    {
        return m_radius;
    }

    public float getTransformedRadius()
    {
        return m_radius * getScaleX();
    }

    public void center()
    {
        setPosition(getX() - m_radius/2, getY() - m_radius/2);
    }
}
