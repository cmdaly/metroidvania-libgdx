package com.bojjenclon.metroidvania.collision.shapes;

import com.bojjenclon.metroidvania.collision.Collision2D;
import com.bojjenclon.metroidvania.collision.data.CollisionData;
import com.bojjenclon.metroidvania.collision.data.RayData;
import com.bojjenclon.metroidvania.collision.math.Vector;

/**
 * Created by Cornell on 12/26/2014.
 */
public class Polygon extends Shape
{
    private Vector[] m_transformedVertices;
    private Vector[] m_vertices;

    public Polygon(float p_x, float p_y, Vector[] p_vertices)
    {
        super(p_x, p_y);

        name = (p_vertices.length + "polygon");

        m_transformedVertices = new Vector[p_vertices.length];
        m_vertices = p_vertices;

        // primarily for debug purpose, may want to remove later
        float l_xMin = 0x7FFFFFFF;
        float l_xMax = -0x7FFFFFFF;
        float l_yMin = 0x7FFFFFFF;
        float l_yMax = -0x7FFFFFFF;

        for (Vector l_vertex : m_vertices)
        {
            if (l_vertex.x < l_xMin)
            {
                l_xMin = l_vertex.x;
            }
            else if (l_vertex.x > l_xMax)
            {
                l_xMax = l_vertex.x;
            }

            if (l_vertex.y < l_yMin)
            {
                l_yMin = l_vertex.y;
            }
            else if (l_vertex.y > l_yMax)
            {
                l_yMax = l_vertex.y;
            }
        }

        setSize(l_xMax - l_xMin, l_yMax - l_yMin);
    }

    public static Polygon create(float p_x, float p_y, int p_sides)
    {
        return create(p_x, p_y, p_sides, 100);
    }

    public static Polygon create(float p_x, float p_y, int p_sides, float p_radius)
    {
        if (p_sides < 3)
        {
            //throw 'Polygon - Needs at least 3 sides';
            return null;
        }

        float l_rotation = (float) ((Math.PI * 2) / p_sides);
        Vector[] l_vertices = new Vector[p_sides];

        for (int i = 0; i < p_sides; i++)
        {
            float l_angle = (float) ((i * l_rotation) + ((Math.PI - l_rotation) * 0.5));
            ;
            Vector l_vector = new Vector();
            l_vector.x = (float) (Math.cos(l_angle) * p_radius);
            l_vector.y = (float) (Math.sin(l_angle) * p_radius);
            l_vertices[i] = l_vector;
        }

        return new Polygon(p_x, p_y, l_vertices);
    }

    public static Polygon rectangle(float p_x, float p_y, float p_width, float p_height)
    {
        return rectangle(p_x, p_y, p_width, p_height, true);
    }

    public static Polygon rectangle(float p_x, float p_y, float p_width, float p_height, boolean p_centered)
    {
        Vector[] l_vertices = new Vector[4];

        if (p_centered)
        {

            l_vertices[0] = new Vector(-p_width / 2, -p_height / 2);
            l_vertices[1] = new Vector(p_width / 2, -p_height / 2);
            l_vertices[2] = new Vector(p_width / 2, p_height / 2);
            l_vertices[3] = new Vector(-p_width / 2, p_height / 2);

        }
        else
        {

            l_vertices[0] = new Vector(0, 0);
            l_vertices[1] = new Vector(p_width, 0);
            l_vertices[2] = new Vector(p_width, p_height);
            l_vertices[3] = new Vector(0, p_height);
        }

        return new Polygon(p_x, p_y, l_vertices);
    }

    public static Polygon square(float p_x, float p_y, float p_width)
    {
        return square(p_x, p_y, p_width, true);
    }

    public static Polygon square(float p_x, float p_y, float p_width, boolean p_centered)
    {
        return rectangle(p_x, p_y, p_width, p_width, p_centered);
    }

    public static Polygon triangle(float p_x, float p_y, float p_radius)
    {
        return create(p_x, p_y, 3, p_radius);
    }

    @Override
    public CollisionData test(Shape p_shape)
    {
        return p_shape.testPolygon(this, true);
    }

    @Override
    public CollisionData testCircle(Circle p_circle, boolean p_flip)
    {
        return Collision2D.testCircleVsPolygon(p_circle, this, p_flip);
    }

    @Override
    public CollisionData testPolygon(Polygon p_polygon, boolean p_flip)
    {
        return Collision2D.testPolygons(this, p_polygon, p_flip);
    }

    @Override
    public RayData testRay(Ray p_ray)
    {
        return Collision2D.rayPolygon(p_ray, this);
    }

    public Vector[] getTransformedVertices()
    {
        if (!m_transformed)
        {
            m_transformedVertices = new Vector[m_vertices.length];
            m_transformed = true;

            for (int i = 0; i < m_vertices.length; i++)
            {
                m_transformedVertices[i] = m_vertices[i].clone().transform(m_transformMatrix);
            }
        }

        return m_transformedVertices;
    }

    public Vector[] getVertices()
    {
        return m_vertices;
    }

    public int getNumSides()
    {
        return m_vertices.length;
    }

    @Override
    public void destroy()
    {
        for (int i = 0; i < m_vertices.length; i++)
        {
            m_vertices[i] = null;
        }

        m_transformedVertices = null;
        m_vertices = null;

        super.destroy();
    }
}
