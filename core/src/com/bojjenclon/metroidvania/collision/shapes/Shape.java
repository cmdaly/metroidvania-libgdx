package com.bojjenclon.metroidvania.collision.shapes;

import com.bojjenclon.metroidvania.collision.data.CollisionData;
import com.bojjenclon.metroidvania.collision.data.RayData;
import com.bojjenclon.metroidvania.collision.math.Matrix;
import com.bojjenclon.metroidvania.collision.math.Vector;

import java.util.HashMap;

/**
 * Created by Cornell on 12/26/2014.
 */
public class Shape
{
    public boolean active = true;
    public String name = "shape";
    public Object data;
    public HashMap<String, String> tags;

    private Vector m_position;
    private float m_rotation = 0;
    private float m_radians = 0;
    private Vector m_scale;

    private float m_scaleX = 1;
    private float m_scaleY = 1;

    private float m_width = 0;
    private float m_height = 0;

    protected boolean m_transformed = false;
    protected Matrix m_transformMatrix;

    public Shape(float p_x, float p_y)
    {
        tags = new HashMap<String, String>();

        m_position = new Vector(p_x, p_y);
        m_scale = new Vector(1, 1);
        m_rotation = 0;

        m_scaleX = 0;
        m_scaleY = 0;

        m_transformMatrix = new Matrix();
        m_transformMatrix.makeTranslation(m_position.x, m_position.y);
    }

    public CollisionData test(Shape p_shape)
    {
        return null;
    }

    public CollisionData testCircle(Circle p_circle)
    {
        return testCircle(p_circle, false);
    }

    public CollisionData testCircle(Circle p_circle, boolean p_flip)
    {
        return null;
    }

    public CollisionData testPolygon(Polygon p_polygon)
    {
        return testPolygon(p_polygon, false);
    }

    public CollisionData testPolygon(Polygon p_polygon, boolean p_flip)
    {
        return null;
    }

    public RayData testRay(Ray p_ray)
    {
        return null;
    }

    private void refreshTransform()
    {
        m_transformMatrix.rotate(m_radians);
        m_transformMatrix.compose(m_position, m_rotation, m_scale);

        m_transformed = false;
    }

    public Vector getPosition()
    {
        return m_position;
    }

    public Vector setPosition(Vector p_position)
    {
        m_position = p_position;

        refreshTransform();

        return m_position;
    }

    public Vector setPosition(float p_x, float p_y)
    {
        m_position.x = p_x;
        m_position.y = p_y;

        refreshTransform();

        return m_position;
    }

    public float getX()
    {
        return m_position.x;
    }

    public float setX(float p_x)
    {
        m_position.x = p_x;

        refreshTransform();

        return m_position.x;
    }

    public float getY()
    {
        return m_position.y;
    }

    public float setY(float p_y)
    {
        m_position.y = p_y;

        refreshTransform();

        return m_position.y;
    }

    public float getRotation()
    {
        return m_rotation;
    }

    public float setRotation(float p_rotation)
    {
        m_radians = (float) (p_rotation * (Math.PI / 180));

        refreshTransform();

        m_rotation = p_rotation;

        return m_rotation;
    }

    public float getScaleX()
    {
        return m_scaleX;
    }

    public float setScaleX(float p_scaleX)
    {
        m_scaleX = p_scaleX;
        m_scale.x = m_scaleX;

        refreshTransform();

        return m_scaleX;
    }

    public float getScaleY()
    {
        return m_scaleY;
    }

    public float setScaleY(float p_scaleY)
    {
        m_scaleY = p_scaleY;
        m_scale.y = m_scaleY;

        refreshTransform();

        return m_scaleY;
    }

    public float getWidth()
    {
        return m_width;
    }

    public float getHeight()
    {
        return m_height;
    }

    // primarily for debug purpose, may want to remove later
    protected void setSize(float p_width, float p_height)
    {
        m_width = p_width;
        m_height = p_height;
    }

    public void destroy()
    {
        m_position = null;
        m_scale = null;
        m_transformMatrix = null;
    }
}
