package com.bojjenclon.metroidvania.collision;

import com.bojjenclon.metroidvania.collision.data.CollisionData;
import com.bojjenclon.metroidvania.collision.data.RayData;
import com.bojjenclon.metroidvania.collision.data.RayIntersectionData;
import com.bojjenclon.metroidvania.collision.math.Common;
import com.bojjenclon.metroidvania.collision.math.Vector;
import com.bojjenclon.metroidvania.collision.shapes.Circle;
import com.bojjenclon.metroidvania.collision.shapes.Polygon;
import com.bojjenclon.metroidvania.collision.shapes.Ray;

/**
 * Created by Cornell on 12/26/2014.
 */
public abstract class Collision2D
{
    public static CollisionData testCircleVsPolygon(Circle p_circle, Polygon p_polygon, boolean p_flip)
    {
        float l_test1, l_test2, l_test;
        float l_min1 = 0;
        float l_max1 = 0x3FFFFFFF;
        float l_min2 = 0;
        float l_max2 = 0x3FFFFFFF;

        Vector l_normalAxis = new Vector();
        float l_offset;
        Vector l_vectorOffset = new Vector();
        Vector[] l_vectors;
        float l_shortestDistance = 0x3FFFFFFF;
        CollisionData l_collisionData = new CollisionData();
        float l_distMin;

        float l_distance = 0xFFFFFFFF;
        float l_testDistance = 0xFFFFFFFF;
        Vector l_closestVector = new Vector();

        l_vectorOffset = new Vector(-p_circle.getX(), -p_circle.getY());

        l_vectors = new Vector[p_polygon.getTransformedVertices().length];
        System.arraycopy(p_polygon.getTransformedVertices(), 0, l_vectors, 0, l_vectors.length);

        // adds some padding to make it more accurate
        if (l_vectors.length == 2)
        {
            Vector l_temp = new Vector(-(l_vectors[1].y - l_vectors[0].y), l_vectors[1].x - l_vectors[0].x);
            l_temp.truncate(0.0000000001f);
            l_vectors = new Vector[]{l_vectors[0], l_vectors[1], l_temp};
        }

        for (int i = 0; i < l_vectors.length; i++)
        {
            l_distance = (p_circle.getX() - (l_vectors[i].x)) * (p_circle.getX() - (l_vectors[i].x)) +
                         (p_circle.getY() - (l_vectors[i].y)) * (p_circle.getY() - (l_vectors[i].y));

            if (l_distance < l_testDistance)
            { //closest has the lowest distance
                l_testDistance = l_distance;
                l_closestVector.x = l_vectors[i].x;
                l_closestVector.y = l_vectors[i].y;
            }
        }

        //get the normal vector
        l_normalAxis = new Vector(l_closestVector.x - p_circle.getX(), l_closestVector.y - p_circle.getY());
        l_normalAxis.normalize(); //normalize is(set its length to 1)

        // project the polygon's points
        l_min1 = l_normalAxis.dot(l_vectors[0]);
        l_max1 = l_min1; //set max and min

        for (int i = 1; i < l_vectors.length; i++)
        {
            l_test = l_normalAxis.dot(l_vectors[i]); //dot to project

            if (l_test < l_min1)
            {
                l_min1 = l_test;
            } //smallest min is wanted

            if (l_test > l_max1)
            {
                l_max1 = l_test;
            } //largest max is wanted
        }

        // project the circle
        l_max2 = p_circle.getTransformedRadius(); //max is radius
        l_min2 -= p_circle.getTransformedRadius(); //min is negative radius

        // offset the polygon's max/min
        l_offset = l_normalAxis.dot(l_vectorOffset);
        l_min1 += l_offset;
        l_max1 += l_offset;

        // do the big test
        l_test1 = l_min1 - l_max2;
        l_test2 = l_min2 - l_max1;

        if (l_test1 > 0 || l_test2 > 0)
        { //if either test is greater than 0, there is a gap, we can give up now.
            return null;
        }

        // circle distance check
        l_distMin = -(l_max2 - l_min1);
        if (p_flip)
        {
            l_distMin *= -1;
        }

        if (Math.abs(l_distMin) < l_shortestDistance)
        {
            l_collisionData.unitVector = l_normalAxis;
            l_collisionData.overlap = l_distMin;
            l_shortestDistance = Math.abs(l_distMin);
        }

        for (int i = 0; i < l_vectors.length; i++)
        {
            l_normalAxis = Common.findNormalAxis(l_vectors, i);

            // project the polygon(again? yes, circles vs. polygon require more testing...)
            l_min1 = l_normalAxis.dot(l_vectors[0]); //project
            l_max1 = l_min1; //set max and min

            //project all the other points(see, cirlces v. polygons use lots of this...)
            for (int j = 1; j < l_vectors.length; j++)
            {
                l_test = l_normalAxis.dot(l_vectors[j]); //more projection
                if (l_test < l_min1)
                {
                    l_min1 = l_test;
                } //smallest min
                if (l_test > l_max1)
                {
                    l_max1 = l_test;
                } //largest max
            }

            // project the circle(again)
            l_max2 = p_circle.getTransformedRadius(); //max is radius
            l_min2 = -p_circle.getTransformedRadius(); //min is negative radius

            //offset points
            l_offset = l_normalAxis.dot(l_vectorOffset);
            l_min1 += l_offset;
            l_max1 += l_offset;

            // do the test, again
            l_test1 = l_min1 - l_max2;
            l_test2 = l_min2 - l_max1;

            if (l_test1 > 0 || l_test2 > 0)
            {

                //failed.. quit now
                return null;

            }

            l_distMin = -(l_max2 - l_min1);

            if (p_flip)
            {
                l_distMin *= -1;
            }

            if (Math.abs(l_distMin) < l_shortestDistance)
            {
                l_collisionData.unitVector = l_normalAxis;
                l_collisionData.overlap = l_distMin;
                l_shortestDistance = Math.abs(l_distMin);
            }

        }

        //if you made it here, there is a collision!!!!!

        l_collisionData.shape2 = p_flip ? p_polygon : p_circle;
        l_collisionData.shape1 = p_flip ? p_circle : p_polygon;
        l_collisionData.separation = new Vector(-l_collisionData.unitVector.x * l_collisionData.overlap,
                -l_collisionData.unitVector.y * l_collisionData.overlap); //return the separation distance

        if (p_flip)
        {
            l_collisionData.unitVector.invert();
        }

        return l_collisionData;
    }

    public static CollisionData testCircles(Circle p_circle1, Circle p_circle2)
    {
        float l_totalRadius = p_circle1.getTransformedRadius() + p_circle2.getTransformedRadius(); //add both radii together to get the colliding distance
        float l_distanceSquared = (p_circle1.getX() - p_circle2.getX()) * (p_circle1.getX() - p_circle2.getX()) + (p_circle1.getY() - p_circle2.getY()) * (p_circle1.getY() - p_circle2.getY()); //find the distance between the two circles using Pythagorean theorem. No square roots for optimization

        if (l_distanceSquared < l_totalRadius * l_totalRadius)
        { //if your distance is less than the totalRadius square(because distance is squared)
            float l_difference = (float) (l_totalRadius - Math.sqrt(l_distanceSquared)); //find the difference. Square roots are needed here.

            CollisionData l_collisionData = new CollisionData(); //new CollisionData class to hold all the data for this collision
            l_collisionData.shape1 = p_circle1;
            l_collisionData.shape2 = p_circle2;
            l_collisionData.unitVector = new Vector(p_circle1.getX() - p_circle2.getX(), p_circle1.getY() - p_circle2.getY());
            l_collisionData.unitVector.normalize();
            l_collisionData.separation = new Vector(l_collisionData.unitVector.x * l_difference,
                    l_collisionData.unitVector.y * l_difference); //find the movement needed to separate the circles
            l_collisionData.overlap = l_collisionData.separation.getLength();

            return l_collisionData;
        }

        return null;
    }

    public static CollisionData testPolygons(Polygon p_polygon1, Polygon p_polygon2)
    {
        return testPolygons(p_polygon1, p_polygon2, false);
    }

    public static CollisionData testPolygons(Polygon p_polygon1, Polygon p_polygon2, boolean p_flip)
    {
        CollisionData l_result1 = checkPolygons(p_polygon1, p_polygon2, p_flip);
        if (l_result1 == null)
        {
            return null;
        }
        CollisionData l_result2 = checkPolygons(p_polygon2, p_polygon1, p_flip);
        if (l_result2 == null)
        {
            return null;
        }

        if (Math.abs(l_result1.overlap) < Math.abs(l_result2.overlap))
        {
            return l_result1;
        }
        else
        {
            return l_result2;
        }
    }

    public static CollisionData checkPolygons(Polygon p_polygon1, Polygon p_polygon2, boolean p_flip)
    {
        float l_test1, l_test2, l_testNum;
        float l_min1, l_max1, l_min2, l_max2;
        Vector l_axis;
        Vector[] l_vectors1;
        Vector[] l_vectors2;
        float l_shortestDistance = 0x3FFFFFFF;
        CollisionData l_collisionData = new CollisionData();

        l_vectors1 = new Vector[p_polygon1.getTransformedVertices().length];
        l_vectors2 = new Vector[p_polygon2.getTransformedVertices().length];

        System.arraycopy(p_polygon1.getTransformedVertices(), 0, l_vectors1, 0, l_vectors1.length);
        System.arraycopy(p_polygon2.getTransformedVertices(), 0, l_vectors2, 0, l_vectors2.length);

        // add a little padding to make the test work correctly for lines
        if (l_vectors1.length == 2)
        {
            Vector l_temp = new Vector(-(l_vectors1[1].y - l_vectors1[0].y), l_vectors1[1].x - l_vectors1[0].x);
            l_temp.truncate(0.0000000001f);
            l_vectors1 = new Vector[]{l_vectors1[0], l_vectors1[1], l_temp};
        }

        if (l_vectors2.length == 2)
        {
            Vector l_temp = new Vector(-(l_vectors2[1].y - l_vectors2[0].y), l_vectors2[1].x - l_vectors2[0].x);
            l_temp.truncate(0.0000000001f);
            l_vectors2 = new Vector[]{l_vectors2[0], l_vectors2[1], l_temp};
        }

        // loop to begin projection
        for (int i = 0; i < l_vectors1.length; i++)
        {
            l_axis = Common.findNormalAxis(l_vectors1, i);

            // project polygon1
            l_min1 = l_axis.dot(l_vectors1[0]);
            l_max1 = l_min1; //set max and min equal

            for (int j = 1; j < l_vectors1.length; j++)
            {
                l_testNum = l_axis.dot(l_vectors1[j]); //project each point
                if (l_testNum < l_min1)
                {
                    l_min1 = l_testNum;
                } //test for new smallest
                if (l_testNum > l_max1)
                {
                    l_max1 = l_testNum;
                } //test for new largest
            }

            // project polygon2
            l_min2 = l_axis.dot(l_vectors2[0]);
            l_max2 = l_min2; //set 2's max and min

            for (int j = 1; j < l_vectors2.length; j++)
            {
                l_testNum = l_axis.dot(l_vectors2[j]); //project each point
                if (l_testNum < l_min2)
                {
                    l_min2 = l_testNum;
                } //test for new smallest
                if (l_testNum > l_max2)
                {
                    l_max2 = l_testNum;
                } //test for new largest
            }

            // and test if they are touching
            l_test1 = l_min1 - l_max2; //test min1 and max2
            l_test2 = l_min2 - l_max1; //test min2 and max1
            if (l_test1 > 0 || l_test2 > 0)
            { //if they are greater than 0, there is a gap
                return null; //just quit
            }

            float l_distMin = -(l_max2 - l_min1);

            if (p_flip)
            {
                l_distMin *= -1;
            }

            if (Math.abs(l_distMin) < l_shortestDistance)
            {
                l_collisionData.unitVector = l_axis;
                l_collisionData.overlap = l_distMin;
                l_shortestDistance = Math.abs(l_distMin);
            }
        }

        //if you're here, there is a collision

        l_collisionData.shape1 = p_flip ? p_polygon2 : p_polygon1;
        l_collisionData.shape2 = p_flip ? p_polygon1 : p_polygon2;
        l_collisionData.separation = new Vector(-l_collisionData.unitVector.x * l_collisionData.overlap,
                -l_collisionData.unitVector.y * l_collisionData.overlap); //return the separation, apply it to a polygon to separate the two shapes.

        if (p_flip)
        {
            l_collisionData.unitVector.invert();
        }

        return l_collisionData;
    }

    public static RayData rayCircle(Ray p_ray, Circle p_circle)
    {
        return null;
    }

    public static RayData rayPolygon(Ray p_ray, Polygon p_polygon)
    {
        return null;
    }

    public static RayIntersectionData rayRay(Ray p_ray1, Ray p_ray2)
    {
        return null;
    }
}
