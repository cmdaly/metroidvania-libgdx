package com.bojjenclon.metroidvania.collision.grid;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;

import java.util.Iterator;

/**
 * Created by Cornell on 12/26/2014.
 */
public class GridWorld
{
    private int m_worldWidth;
    private int m_worldHeight;

    private int m_gridWidth;
    private int m_gridHeight;

    private GridCell[][] m_grid;

    public GridWorld(int p_worldWidth, int p_worldHeight, int p_gridWidth, int p_gridHeight)
    {
        m_worldWidth = (int) Math.ceil(p_worldWidth / p_gridWidth);
        m_worldHeight = (int) Math.ceil(p_worldHeight / p_gridHeight);

        m_gridWidth = p_gridWidth;
        m_gridHeight = p_gridHeight;

        m_grid = new GridCell[m_gridWidth][m_gridHeight];

        for (int x = 0; x < m_worldWidth; x++)
        {
            for (int y = 0; y < m_worldHeight; y++)
            {
                m_grid[x][y] = new GridCell(new Rectangle(x, y, m_gridWidth, m_gridHeight));
            }
        }
    }

    public void addEntity(Entity p_entity, Rectangle p_area)
    {
        Vector2 l_origin = new Vector2(p_area.width / 2.0f, p_area.height / 2.0f);

        Rectangle l_trueArea = new Rectangle(p_area);
        l_trueArea.x -= l_origin.x;
        l_trueArea.y -= l_origin.y;

        Vector2 l_difference = new Vector2(l_trueArea.x % m_gridWidth, l_trueArea.y % m_gridHeight);

        int l_horizontalParts = (int) Math.ceil((l_trueArea.width + l_difference.x) / m_gridWidth);
        int l_verticalParts = (int) Math.ceil((l_trueArea.height + l_difference.y) / m_gridHeight);

        Vector2 l_coords = new Vector2(l_trueArea.x, l_trueArea.y);

        for (int i = 0; i < l_horizontalParts; i++)
        {
            l_coords.y = l_trueArea.y;

            for (int j = 0; j < l_verticalParts; j++)
            {
                Vector2 l_gridCoords = worldToGridCoords(l_coords);
                GridCell l_cell = m_grid[(int) l_gridCoords.x][(int) l_gridCoords.y];

                l_cell.add(p_entity);

                l_coords.y += m_gridHeight;
            }

            l_coords.x += m_gridWidth;
        }
    }

    public void removeEntity(Entity p_entity, Rectangle p_area)
    {
        Vector2 l_origin = new Vector2(p_area.width / 2.0f, p_area.height / 2.0f);

        Rectangle l_trueArea = new Rectangle(p_area);
        l_trueArea.x -= l_origin.x;
        l_trueArea.y -= l_origin.y;

        Vector2 l_difference = new Vector2(l_trueArea.x % m_gridWidth, l_trueArea.y % m_gridHeight);

        int l_horizontalParts = (int) Math.ceil((l_trueArea.width + l_difference.x) / m_gridWidth);
        int l_verticalParts = (int) Math.ceil((l_trueArea.height + l_difference.y) / m_gridHeight);

        Vector2 l_coords = new Vector2(l_trueArea.x, l_trueArea.y);

        for (int i = 0; i < l_horizontalParts; i++)
        {
            l_coords.y = l_trueArea.y;

            for (int j = 0; j < l_verticalParts; j++)
            {
                Vector2 l_gridCoords = worldToGridCoords(l_coords);
                GridCell l_cell = m_grid[(int) l_gridCoords.x][(int) l_gridCoords.y];

                l_cell.remove(p_entity);

                l_coords.y += m_gridHeight;
            }

            l_coords.x += m_gridWidth;
        }
    }

    public void updateEntity(Entity p_entity, Rectangle p_area, Vector3 p_previousPos)
    {
        if (p_area.x == p_previousPos.x && p_area.y == p_previousPos.y)
        {
            return;
        }

        Vector2 l_origin = new Vector2(p_area.width / 2.0f, p_area.height / 2.0f);

        // remove from old cells

        Rectangle l_trueArea = new Rectangle(
                p_previousPos.x - l_origin.x , p_previousPos.y - l_origin.y,
                p_area.width, p_area.height
        );
        Vector2 l_difference = new Vector2(l_trueArea.x % m_gridWidth, l_trueArea.y % m_gridHeight);

        int l_horizontalParts = (int) Math.ceil((l_trueArea.width + l_difference.x) / m_gridWidth);
        int l_verticalParts = (int) Math.ceil((l_trueArea.height + l_difference.y) / m_gridHeight);

        Vector2 l_coords = new Vector2(l_trueArea.x, l_trueArea.y);

        for (int i = 0; i < l_horizontalParts; i++)
        {
            l_coords.y = l_trueArea.y;

            for (int j = 0; j < l_verticalParts; j++)
            {
                Vector2 l_gridCoords = worldToGridCoords(l_coords);
                GridCell l_cell = m_grid[(int) l_gridCoords.x][(int) l_gridCoords.y];

                l_cell.remove(p_entity);

                l_coords.y += m_gridHeight;
            }

            l_coords.x += m_gridWidth;
        }

        // add to new cells

        l_trueArea = new Rectangle(p_area);
        l_trueArea.x -= l_origin.x;
        l_trueArea.y -= l_origin.y;

        l_difference = new Vector2(l_trueArea.x % m_gridWidth, l_trueArea.y % m_gridHeight);

        l_horizontalParts = (int) Math.ceil((l_trueArea.width + l_difference.x) / m_gridWidth);
        l_verticalParts = (int) Math.ceil((l_trueArea.height + l_difference.y) / m_gridHeight);

        l_coords = new Vector2(l_trueArea.x, l_trueArea.y);

        for (int i = 0; i < l_horizontalParts; i++)
        {
            l_coords.y = l_trueArea.y;

            for (int j = 0; j < l_verticalParts; j++)
            {
                Vector2 l_gridCoords = worldToGridCoords(l_coords);
                GridCell l_cell = m_grid[(int) l_gridCoords.x][(int) l_gridCoords.y];

                l_cell.add(p_entity);

                l_coords.y += m_gridHeight;
            }

            l_coords.x += m_gridWidth;
        }
    }

    public GridCell getCell(float p_x, float p_y)
    {
        int l_x = (int) p_x;
        int l_y = (int) p_y;

        return m_grid[l_x][l_y];
    }

    public Array<Entity> getEntitiesInArea(Rectangle p_area)
    {
        Vector2 l_origin = new Vector2(p_area.width / 2.0f, p_area.height / 2.0f);

        Rectangle l_trueArea = new Rectangle(p_area);
        l_trueArea.x -= l_origin.x;
        l_trueArea.y -= l_origin.y;

        Vector2 l_difference = new Vector2(l_trueArea.x % m_gridWidth, l_trueArea.y % m_gridHeight);

        int l_horizontalParts = (int) Math.ceil((l_trueArea.width + l_difference.x) / m_gridWidth);
        int l_verticalParts = (int) Math.ceil((l_trueArea.height + l_difference.y) / m_gridHeight);

        Vector2 l_coords = new Vector2(l_trueArea.x, l_trueArea.y);

        Array<Entity> l_entities = new Array<Entity>();
        for (int i = 0; i < l_horizontalParts; i++)
        {
            l_coords.y = l_trueArea.y;

            for (int j = 0; j < l_verticalParts; j++)
            {
                Vector2 l_gridCoords = worldToGridCoords(l_coords);
                GridCell l_cell = m_grid[(int) l_gridCoords.x][(int) l_gridCoords.y];

                Iterator<Entity> l_iterator = l_cell.iterator();
                while (l_iterator.hasNext())
                {
                    Entity l_entity = l_iterator.next();

                    if (!l_entities.contains(l_entity, true))
                    {
                        l_entities.add(l_entity);
                    }
                }

                l_coords.y += m_gridHeight;
            }

            l_coords.x += m_gridWidth;
        }

        return l_entities;
    }

    public void reset()
    {
        for (GridCell[] l_row : m_grid)
        {
            for (GridCell l_cell : l_row)
            {
                l_cell.reset();
            }
        }
    }

    public int getWidth()
    {
        return m_worldWidth;
    }

    public int getHeight()
    {
        return m_worldHeight;
    }

    private Vector2 worldToGridCoords(Vector2 p_coordinates)
    {
        int l_x = (int) Math.floor(p_coordinates.x / m_gridWidth);
        int l_y = (int) Math.floor(p_coordinates.y / m_gridHeight);

        if (l_x < 0)
        {
            l_x = 0;
        }
        else if (l_x >= m_worldWidth)
        {
            l_x = (m_worldWidth - 1);
        }

        if (l_y < 0)
        {
            l_y = 0;
        }
        else if (l_y >= m_worldHeight)
        {
            l_y = (m_worldHeight - 1);
        }

        return new Vector2(l_x, l_y);
    }
}
