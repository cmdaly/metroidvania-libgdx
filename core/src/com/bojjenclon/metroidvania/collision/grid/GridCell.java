package com.bojjenclon.metroidvania.collision.grid;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.math.Rectangle;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Cornell on 12/26/2014.
 */
public class GridCell
{
    private Rectangle m_cell;
    private ArrayList<Entity> m_contents;

    public GridCell(Rectangle p_cell)
    {
        m_cell = p_cell;
        m_contents = new ArrayList<Entity>();
    }

    public boolean add(Entity p_entity)
    {
        if (m_contents.contains(p_entity))
        {
            return false;
        }

        m_contents.add(p_entity);

        return true;
    }

    public boolean remove(Entity p_entity)
    {
        if (!m_contents.contains(p_entity))
        {
            return false;
        }

        m_contents.remove(p_entity);

        return true;
    }

    public Iterator<Entity> iterator()
    {
        return m_contents.iterator();
    }

    public int size()
    {
        return m_contents.size();
    }

    public void reset()
    {
        m_contents.clear();
    }
}
