package com.bojjenclon.metroidvania.collision.math;

/**
 * Created by Cornell on 12/26/2014.
 */
public abstract class Common
{
    public static Vector findNormalAxis(Vector[] p_vertices, int p_index)
    {
        Vector l_vector1 = p_vertices[p_index];
        Vector l_vector2 = (p_index >= p_vertices.length - 1) ? p_vertices[0] : p_vertices[p_index + 1];

        Vector l_normalAxis = new Vector(-(l_vector2.y - l_vector1.y), l_vector2.x - l_vector1.x);
        l_normalAxis.normalize();

        return l_normalAxis;
    }
}
