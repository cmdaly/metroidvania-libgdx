package com.bojjenclon.metroidvania.collision.math;

/**
 * Created by Cornell on 12/26/2014.
 */
public class Vector
{
    public float x = 0;
    public float y = 0;

    private float m_length;

    public Vector()
    {
        this(0, 0);
    }

    public Vector(float p_x, float p_y)
    {
        x = p_x;
        y = p_y;
    }

    public Vector clone()
    {
        return new Vector(x, y);
    }

    public Vector transform(Matrix p_matrix)
    {
        Vector l_vec = clone();

        l_vec.x = (x * p_matrix.a + y * p_matrix.c + p_matrix.tx);
        l_vec.y = (x * p_matrix.b + y * p_matrix.d + p_matrix.ty);

        return l_vec;
    }

    public float setLength(float p_length)
    {
        m_length = p_length;

        float l_angle = (float) Math.atan2(y, x);

        x = (float) (Math.cos(l_angle) * p_length);
        y = (float) (Math.sin(l_angle) * p_length);

        if (Math.abs(x) < 0.00000001)
        {
            x = 0;
        }

        if (Math.abs(y) < 0.00000001)
        {
            y = 0;
        }

        return m_length;
    }

    public float getLength()
    {
        return (float) Math.sqrt(getLengthSq());
    }

    public float getLengthSq()
    {
        return x * x + y * y;
    }

    public Vector normalize()
    {
        if (getLength() == 0)
        {
            x = 1;

            return this;
        }

        float l_length = getLength();

        x /= l_length;
        y /= l_length;

        return this;
    }

    public Vector truncate(float p_max)
    {
        setLength(Math.min(p_max, getLength()));

        return this;
    }

    public Vector invert()
    {
        x = -x;
        y = -y;

        return this;
    }

    public float dot(Vector p_vector)
    {
        return x * p_vector.x + y * p_vector.y;
    }

    public float cross(Vector p_vector)
    {
        return x * p_vector.y - y * p_vector.x;
    }

    public Vector add(Vector p_vector)
    {
        x += p_vector.x;
        y += p_vector.y;

        return this;
    }

    public Vector subtract(Vector p_vector)
    {
        x -= p_vector.x;
        y -= p_vector.y;

        return this;
    }

    public String toString()
    {
        return "Vector x:" + x + ", y:" + y;
    }
}
