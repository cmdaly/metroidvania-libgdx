package com.bojjenclon.metroidvania.collision.math;

/**
 * Created by Cornell on 12/26/2014.
 */
public class Matrix
{
    public float a;
    public float b;
    public float c;
    public float d;
    public float tx;
    public float ty;

    private float m_lastRotation;

    public Matrix()
    {
        this(1, 0, 0, 1, 0, 0);
    }

    public Matrix(float p_a, float p_b, float p_c, float p_d, float p_tx, float p_ty)
    {
        a = p_a;
        b = p_b;
        c  = p_c;
        d = p_d;
        tx = p_tx;
        ty = p_ty;
    }

    public void identity()
    {
        a = 1;
        b = 0;
        c  = 0;
        d = 1;
        tx = 0;
        ty = 0;
    }

    public void translate(float p_x, float p_y)
    {
        tx += p_x;
        ty += p_y;
    }

    public void compose(Vector p_position, float p_rotation, Vector p_scale)
    {
        float l_diff = (p_rotation - m_lastRotation);

        identity();

        scale(p_scale.x, p_scale.y);
        rotate(l_diff);
        makeTranslation(p_position.x, p_position.y);

        m_lastRotation = p_rotation;
    }

    public Matrix makeTranslation(float p_x, float p_y)
    {
        tx = p_x;
        ty = p_y;

        return this;
    }

    public void rotate(float p_angle)
    {
        float l_cos = (float) Math.cos(p_angle);
        float l_sin = (float) Math.sin(p_angle);

        float l_a1 = (a * l_cos - b * l_sin);
        b = a * l_sin + b * l_cos;
        a = l_a1;

        float l_c1 = (c * l_cos - d * l_sin);
        d = (c * l_sin + d * l_cos);
        c = l_c1;

        float l_tx1 = (tx * l_cos - ty * l_sin);
        ty = (tx * l_sin + ty * l_cos);
        tx = l_tx1;
    }

    public void scale(float p_x, float p_y)
    {
        a *= p_x;
        b *= p_y;

        c *= p_x;
        d *= p_y;

        tx *= p_x;
        ty *= p_y;
    }

    public String toString()
    {
        return "(a=" + a + ", b=" + b + ", c=" + c + ", d=" + d + ", tx=" + tx + ", ty=" + ty + ")";
    }
}
