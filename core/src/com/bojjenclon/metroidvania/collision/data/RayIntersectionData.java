package com.bojjenclon.metroidvania.collision.data;

import com.bojjenclon.metroidvania.collision.shapes.Ray;

/**
 * Created by Cornell on 12/26/2014.
 */
public class RayIntersectionData
{
    public Ray ray1;
    public Ray ray2;

    public float u1;
    public float u2;

    public RayIntersectionData(Ray p_ray1, Ray p_ray2, float p_u1, float p_u2)
    {
        ray1 = p_ray1;
        ray2 = p_ray2;

        u1 = p_u1;
        u2 = p_u2;
    }
}
