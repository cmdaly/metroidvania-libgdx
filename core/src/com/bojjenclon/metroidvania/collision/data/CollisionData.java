package com.bojjenclon.metroidvania.collision.data;

import com.bojjenclon.metroidvania.collision.math.Vector;
import com.bojjenclon.metroidvania.collision.shapes.Shape;

/**
 * Created by Cornell on 12/26/2014.
 */
public class CollisionData
{
    public float overlap = 0;
    public Vector separation;

    public Shape shape1;
    public Shape shape2;

    public Vector unitVector;

    public CollisionData()
    {
        separation = new Vector();
        unitVector = new Vector();
    }
}
