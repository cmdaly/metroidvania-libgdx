package com.bojjenclon.metroidvania.collision.data;

import com.bojjenclon.metroidvania.collision.shapes.Ray;
import com.bojjenclon.metroidvania.collision.shapes.Shape;

/**
 * Created by Cornell on 12/26/2014.
 */
public class RayData
{
    public Shape shape;
    public Ray ray;

    public float start;
    public float end;

    public RayData(Shape p_shape, Ray p_ray, float p_start, float p_end)
    {
        shape = p_shape;
        ray = p_ray;

        start = p_start;
        end = p_end;
    }
}
