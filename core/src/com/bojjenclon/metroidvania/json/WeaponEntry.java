package com.bojjenclon.metroidvania.json;

/**
 * Created by Cornell on 12/30/2014.
 */
public class WeaponEntry
{
    public String name;
    public String gfx;
    public float x;
    public float y;
    public int depth;
    public int damage;
}
