package com.bojjenclon.metroidvania.json;

import com.badlogic.gdx.utils.Array;

/**
 * Created by Cornell on 12/30/2014.
 */
public class WeaponData
{
    public Array<WeaponEntry> weapons;
}
