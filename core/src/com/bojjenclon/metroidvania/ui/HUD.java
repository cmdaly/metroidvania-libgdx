package com.bojjenclon.metroidvania.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.bojjenclon.metroidvania.Config;

/**
 * Created by Cornell on 1/6/2015.
 */
public class HUD
{
    private Stage m_stage;
    private Skin m_skin;

    private SegmentedBar m_hpBar;
    private Label m_hpLabel;

    public HUD()
    {
        m_stage = new Stage(new FitViewport(Config.WIDTH, Config.HEIGHT));
        m_skin = new Skin(Gdx.files.internal("skins/uiskin.json"));
        Gdx.input.setInputProcessor(m_stage);

        setup();
    }

    public void render(float p_delta)
    {
        m_stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30.0f));
        m_stage.draw();
    }

    public void resize(int p_width, int p_height)
    {
        m_stage.getViewport().update(p_width, p_height, true);
    }

    public void dispose()
    {
        m_skin.dispose();
        m_stage.dispose();
    }

    public void updateHealthBar(int p_hp, int p_maxHP)
    {
        float l_percent = (1.0f * p_hp / p_maxHP);

        m_hpBar.setProgress(l_percent);
        m_hpLabel.setText("HP: " + p_hp + "/" + p_maxHP);
    }

    private void setup()
    {
        Table l_table = new Table();
        l_table.setFillParent(true);
        m_stage.addActor(l_table);

        /*final TextButton l_button = new TextButton("Click me!", m_skin);
        l_table.add(l_button);

        l_button.addListener(new ChangeListener() {
            public void changed (ChangeEvent event, Actor actor) {
                System.out.println("Clicked! Is checked: " + l_button.isChecked());
                l_button.setText("Good job!");
            }
        });*/

        m_hpBar = new SegmentedBar("red", 80.0f);
        l_table.add(m_hpBar);

        m_hpLabel = new Label("HP: 100%", m_skin);
        l_table.add(m_hpLabel);

        l_table.left().top();
    }
}
