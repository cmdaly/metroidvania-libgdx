package com.bojjenclon.metroidvania.ui;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.bojjenclon.metroidvania.Global;

/**
 * Created by Cornell on 1/8/2015.
 */
public class SegmentedBar extends Actor
{
    private TextureRegion m_bgLeftCap;
    private TextureRegion m_bgRightCap;
    private TextureRegion m_bgMiddle;

    private TextureRegion m_fgLeftCap;
    private TextureRegion m_fgRightCap;
    private TextureRegion m_fgMiddle;

    private float m_width;
    private float m_trueWidth;
    private float m_progress;
    private float m_incrementSize;
    private float m_capLimit;

    public SegmentedBar(String p_color, float p_width)
    {
        super();

        TextureAtlas l_atlas = Global.assets.get("gfx/ui.txt", TextureAtlas.class);

        m_bgLeftCap = l_atlas.findRegion("barBack_horizontalLeft");
        m_bgRightCap = l_atlas.findRegion("barBack_horizontalRight");
        m_bgMiddle = l_atlas.findRegion("barBack_horizontalMid");

        String l_fixedColor = (p_color.substring(0, 1).toUpperCase() + p_color.substring(1).toLowerCase());

        m_fgLeftCap = l_atlas.findRegion("bar" + l_fixedColor + "_horizontalLeft");
        m_fgRightCap = l_atlas.findRegion("bar" + l_fixedColor + "_horizontalRight");
        m_fgMiddle = l_atlas.findRegion("bar" + l_fixedColor + "_horizontalMid");

        m_width = p_width;
        m_progress = 1.0f;
        m_incrementSize = (m_width / m_fgMiddle.getRegionWidth() / 100.0f);
        m_capLimit = (1.0f - m_incrementSize);
        m_trueWidth = (float) Math.ceil(m_width + (m_width * m_incrementSize * 2));

        this.setSize(
                m_bgLeftCap.getRegionWidth() + m_bgRightCap.getRegionWidth() + m_width,
                m_bgLeftCap.getRegionHeight()
        );
    }

    @Override
    public void draw(Batch p_batch, float p_parentAlpha)
    {
        // draw background bar
        p_batch.draw(
                m_bgLeftCap,
                getX(), getY(),
                m_bgLeftCap.getRegionWidth() * getScaleX(), m_bgLeftCap.getRegionHeight() * getScaleY()
        );
        p_batch.draw(
                m_bgMiddle,
                getX() + m_bgLeftCap.getRegionWidth(), getY(),
                m_width * getScaleX(), m_bgMiddle.getRegionHeight() * getScaleY()
        );
        p_batch.draw(
                m_bgRightCap,
                getX() + m_bgLeftCap.getRegionWidth() + m_width, getY(),
                m_bgRightCap.getRegionWidth() * getScaleX(), m_bgRightCap.getRegionHeight() * getScaleY()
        );

        // draw foreground bar
        float l_adjustedProgress = (m_progress - (m_incrementSize * 2));
        if (m_progress > 0)
        {
            p_batch.draw(
                    m_fgLeftCap,
                    getX(), getY(),
                    m_fgLeftCap.getRegionWidth() * getScaleX(), m_bgLeftCap.getRegionHeight() * getScaleY()
            );
        }
        if (l_adjustedProgress > m_incrementSize)
        {
            p_batch.draw(
                    m_fgMiddle,
                    getX() + m_fgLeftCap.getRegionWidth(), getY(),
                    m_trueWidth * l_adjustedProgress * getScaleX(), m_fgMiddle.getRegionHeight() * getScaleY()
            );
        }
        if (m_progress >= m_capLimit)
        {
            p_batch.draw(
                    m_fgRightCap,
                    getX() + m_fgLeftCap.getRegionWidth() + m_width, getY(),
                    m_fgRightCap.getRegionWidth() * getScaleX(), m_fgRightCap.getRegionHeight() * getScaleY()
            );
        }
    }

    public void setProgress(float p_progress)
    {
        m_progress = p_progress;

        if (m_progress < 0)
        {
            m_progress = 0;
        }
        else if (m_progress > 1)
        {
            m_progress = 1;
        }
    }
}
