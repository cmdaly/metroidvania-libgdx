package com.bojjenclon.metroidvania;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.bojjenclon.metroidvania.collision.grid.GridWorld;
import com.bojjenclon.metroidvania.collision.shapes.Polygon;
import com.bojjenclon.metroidvania.collision.shapes.Shape;
import com.bojjenclon.metroidvania.components.*;
import com.bojjenclon.metroidvania.enums.Direction;
import com.bojjenclon.metroidvania.enums.Identifier;
import com.bojjenclon.metroidvania.json.WeaponEntry;

/**
 * Created by Cornell on 12/24/2014.
 */
public class EntityManager
{
    private static Metroidvania s_game;
    private static Engine s_engine;
    private static GridWorld s_grid;

    public static void setGame(Metroidvania p_game)
    {
        s_game = p_game;
    }

    public static void setEngine(Engine p_engine)
    {
        s_engine = p_engine;
    }

    public static void setGrid(GridWorld p_grid)
    {
        s_grid = p_grid;
    }

    public static Entity createPlayer()
    {
        return createPlayer(0, 0);
    }

    public static Entity createPlayer(float p_x, float p_y)
    {
        Entity l_player = new Entity();

        EntityStateComponent l_entityStateComponent = new EntityStateComponent();

        Texture l_texture = Global.assets.get("gfx/battlemage.gif", Texture.class);
        AnimatedImage l_animatedImage = new AnimatedImage(l_texture, 1, 1);
        l_animatedImage.addAnimation("idle", new int[] {0, 0, 0}, 0.025f, true);
        l_animatedImage.addAnimation("walk", new int[] {0, 0, 0}, 0.025f, true);
        l_animatedImage.addAnimation("attack", new int[] {0, 0, 0}, 0.45f, false);
        l_animatedImage.play("idle");

        AnimationComponent l_animationComponent = new AnimationComponent();
        l_animationComponent.animatedImage = l_animatedImage;

        TransformComponent l_transformComponent = new TransformComponent();
        l_transformComponent.pos.set(p_x, p_y, 0);
        l_transformComponent.origin.set(l_texture.getWidth() / 2.0f, l_texture.getHeight() / 2.0f);
        l_transformComponent.size.set(l_texture.getWidth(), l_texture.getHeight());

        VelocityComponent l_velocityComponent = new VelocityComponent();

        IdentifierComponent l_identifierComponent = new IdentifierComponent();
        l_identifierComponent.type = Identifier.Player;

        PlayerComponent l_playerComponent = new PlayerComponent();

        JumpComponent l_jumpComponent = new JumpComponent();

        Shape l_shape = Polygon.rectangle(p_x, p_y, l_texture.getWidth(), l_texture.getHeight());
        ColliderComponent l_colliderComponent = new ColliderComponent();
        l_colliderComponent.shape = l_shape;

        EquipmentComponent l_equipmentComponent = new EquipmentComponent();

        StatsComponent l_statsComponent = new StatsComponent();

        HitDelayComponent l_hitDelayComponent = new HitDelayComponent();

        DashComponent l_dashComponent = new DashComponent();

        DirectionComponent l_directionComponent = new DirectionComponent();

        /* Add All Components */

        l_player.add(l_entityStateComponent);
        l_player.add(l_animationComponent);
        l_player.add(l_transformComponent);
        l_player.add(l_velocityComponent);
        l_player.add(l_identifierComponent);
        l_player.add(l_colliderComponent);
        l_player.add(l_playerComponent);
        l_player.add(l_jumpComponent);
        l_player.add(l_equipmentComponent);
        l_player.add(l_statsComponent);
        l_player.add(l_hitDelayComponent);
        l_player.add(l_dashComponent);
        l_player.add(l_directionComponent);

        Rectangle l_area = new Rectangle(
                p_x - l_transformComponent.origin.x, p_y - l_transformComponent.origin.y,
                l_transformComponent.size.x, l_transformComponent.size.y
        );
        s_grid.addEntity(l_player, l_area);

        return l_player;
    }

    public static Entity createCannibal()
    {
        return createCannibal(0, 0);
    }

    public static Entity createCannibal(float p_x, float p_y)
    {
        Entity l_cannibal = new Entity();

        EntityStateComponent l_entityStateComponent = new EntityStateComponent();

        Texture l_texture = Global.assets.get("gfx/cannibal.gif", Texture.class);
        TextureComponent l_textureComponent = new TextureComponent();
        l_textureComponent.texture = l_texture;

        TransformComponent l_transformComponent = new TransformComponent();
        l_transformComponent.pos.set(p_x, p_y, 0);
        l_transformComponent.origin.set(l_texture.getWidth() / 2.0f, l_texture.getHeight() / 2.0f);
        l_transformComponent.size.set(l_texture.getWidth(), l_texture.getHeight());

        VelocityComponent l_velocityComponent = new VelocityComponent();

        IdentifierComponent l_identifierComponent = new IdentifierComponent();
        l_identifierComponent.type = Identifier.Enemy;

        Shape l_shape = Polygon.rectangle(p_x, p_y, l_texture.getWidth(), l_texture.getHeight());
        ColliderComponent l_colliderComponent = new ColliderComponent();
        l_colliderComponent.shape = l_shape;

        StatsComponent l_statsComponent = new StatsComponent();
        l_statsComponent.hp = l_statsComponent.maxHP = 5;
        l_statsComponent.mp = l_statsComponent.maxMP = 0;

        HitDelayComponent l_hitDelayComponent = new HitDelayComponent();

        DirectionComponent l_directionComponent = new DirectionComponent();

        /* Add All Components */

        l_cannibal.add(l_entityStateComponent);
        l_cannibal.add(l_textureComponent);
        l_cannibal.add(l_transformComponent);
        l_cannibal.add(l_velocityComponent);
        l_cannibal.add(l_identifierComponent);
        l_cannibal.add(l_colliderComponent);
        l_cannibal.add(l_statsComponent);
        l_cannibal.add(l_hitDelayComponent);
        l_cannibal.add(l_directionComponent);

        Rectangle l_area = new Rectangle(
                p_x - l_transformComponent.origin.x, p_y - l_transformComponent.origin.y,
                l_transformComponent.size.x, l_transformComponent.size.y
        );
        s_grid.addEntity(l_cannibal, l_area);

        return l_cannibal;
    }

    public static Entity createFloor(float p_x, float p_y)
    {
        return createFloor(p_x, p_y, 1.0f, 1.0f);
    }

    public static Entity createFloor(float p_x, float p_y, float p_scaleX, float p_scaleY)
    {
        Entity l_floor = new Entity();

        EntityStateComponent l_entityStateComponent = new EntityStateComponent();

        Texture l_texture = Global.assets.get("gfx/roomFloor31.gif", Texture.class);
        TextureComponent l_textureComponent = new TextureComponent();
        l_textureComponent.texture = l_texture;

        TransformComponent l_transformComponent = new TransformComponent();
        l_transformComponent.pos.set(p_x, p_y, 0);
        l_transformComponent.origin.set(l_texture.getWidth() / 2.0f, l_texture.getHeight() / 2.0f);
        l_transformComponent.scale.set(p_scaleX, p_scaleY);
        l_transformComponent.size.set(l_texture.getWidth(), l_texture.getHeight());

        IdentifierComponent l_identifierComponent = new IdentifierComponent();
        l_identifierComponent.type = Identifier.Floor;

        Shape l_shape = Polygon.rectangle(p_x, p_y, l_texture.getWidth(), l_texture.getHeight());
        ColliderComponent l_colliderComponent = new ColliderComponent();
        l_colliderComponent.shape = l_shape;
        l_colliderComponent.isStatic = true;

        /* Add All Components */

        l_floor.add(l_entityStateComponent);
        l_floor.add(l_textureComponent);
        l_floor.add(l_transformComponent);
        l_floor.add(l_colliderComponent);
        l_floor.add(l_identifierComponent);

        Rectangle l_area = new Rectangle(
                p_x - l_transformComponent.origin.x, p_y - l_transformComponent.origin.y,
                l_transformComponent.size.x, l_transformComponent.size.y
        );
        s_grid.addEntity(l_floor, l_area);

        return l_floor;
    }

    public static Entity createWall(float p_x, float p_y)
    {
        return createWall(p_x, p_y, 1.0f, 1.0f);
    }

    public static Entity createWall(float p_x, float p_y, float p_scaleX, float p_scaleY)
    {
        Entity l_wall = new Entity();

        EntityStateComponent l_entityStateComponent = new EntityStateComponent();

        Texture l_texture = Global.assets.get("gfx/wall13.gif", Texture.class);
        TextureComponent l_textureComponent = new TextureComponent();
        l_textureComponent.texture = l_texture;

        TransformComponent l_transformComponent = new TransformComponent();
        l_transformComponent.pos.set(p_x, p_y, 0);
        l_transformComponent.origin.set(l_texture.getWidth() / 2.0f, l_texture.getHeight() / 2.0f);
        l_transformComponent.scale.set(p_scaleX, p_scaleY);
        l_transformComponent.size.set(l_texture.getWidth(), l_texture.getHeight());

        IdentifierComponent l_identifierComponent = new IdentifierComponent();
        l_identifierComponent.type = Identifier.Wall;

        Shape l_shape = Polygon.rectangle(p_x, p_y, l_texture.getWidth(), l_texture.getHeight());
        ColliderComponent l_colliderComponent = new ColliderComponent();
        l_colliderComponent.shape = l_shape;
        l_colliderComponent.isStatic = true;

        /* Add All Components */

        l_wall.add(l_entityStateComponent);
        l_wall.add(l_textureComponent);
        l_wall.add(l_transformComponent);
        l_wall.add(l_colliderComponent);
        l_wall.add(l_identifierComponent);

        Rectangle l_area = new Rectangle(
                p_x - l_transformComponent.origin.x, p_y - l_transformComponent.origin.y,
                l_transformComponent.size.x, l_transformComponent.size.y
        );
        s_grid.addEntity(l_wall, l_area);

        return l_wall;
    }

    public static Entity createWeapon(String p_name, Entity p_parent)
    {
        return createWeapon(p_name, p_parent, false);
    }

    public static Entity createWeapon(String p_name, Entity p_parent, boolean p_addToEngine)
    {
        WeaponEntry l_weaponEntry = Global.weapons.get(p_name);

        if (l_weaponEntry == null)
        {
            return null;
        }

        TransformComponent l_parentTransform = Mappers.transformMapper.get(p_parent);
        Vector3 l_parentPos = l_parentTransform.pos;

        EquipmentComponent l_parentEquipment = Mappers.equipmentMapper.get(p_parent);

        Entity l_weapon;
        if (l_parentEquipment.weaponEntity == null)
        {
            l_weapon = new Entity();

            l_parentEquipment.weaponEntity = l_weapon;
        }
        else
        {
            updateWeapon(p_name, l_parentEquipment.weaponEntity, p_parent);

            return l_parentEquipment.weaponEntity;
        }

        DirectionComponent l_parentDirectionComponent = Mappers.directionMapper.get(p_parent);
        Direction l_parentDirection = l_parentDirectionComponent.direction;

        EntityStateComponent l_entityStateComponent = new EntityStateComponent();

        Texture l_texture = Global.assets.get(l_weaponEntry.gfx, Texture.class);
        TextureComponent l_textureComponent = new TextureComponent();
        l_textureComponent.texture = l_texture;

        TransformComponent l_transformComponent = new TransformComponent();
        l_transformComponent.pos.set(l_parentPos.x + l_weaponEntry.x, l_parentPos.y + l_weaponEntry.y, l_parentPos.z);
        l_transformComponent.origin.set(l_texture.getWidth() / 2.0f, l_texture.getHeight() / 2.0f);
        l_transformComponent.size.set(l_texture.getWidth(), l_texture.getHeight());

        IdentifierComponent l_identifierComponent = new IdentifierComponent();
        l_identifierComponent.type = Identifier.Weapon;

        AnchorComponent l_anchorComponent = new AnchorComponent();
        l_anchorComponent.parent = p_parent;
        if (l_parentDirection == Direction.Right)
        {
            l_anchorComponent.relativePosition.set(l_weaponEntry.x, l_weaponEntry.y, 0);
        }
        else
        {
            l_anchorComponent.relativePosition.set(-l_weaponEntry.x, l_weaponEntry.y, 0);
        }

        Shape l_shape = Polygon.rectangle(
                l_parentPos.x + l_anchorComponent.relativePosition.x,
                l_parentPos.y + l_anchorComponent.relativePosition.y,
                l_texture.getWidth(),
                l_texture.getHeight()
        );
        ColliderComponent l_colliderComponent = new ColliderComponent();
        l_colliderComponent.shape = l_shape;

        WeaponComponent l_weaponComponent = new WeaponComponent();
        l_weaponComponent.damage = l_weaponEntry.damage;

        DirectionComponent l_directionComponent = new DirectionComponent();
        l_directionComponent.direction = l_parentDirection;

        /* Add All Components */

        l_weapon.add(l_entityStateComponent);
        l_weapon.add(l_textureComponent);
        l_weapon.add(l_transformComponent);
        l_weapon.add(l_identifierComponent);
        l_weapon.add(l_colliderComponent);
        l_weapon.add(l_anchorComponent);
        l_weapon.add(l_weaponComponent);
        l_weapon.add(l_directionComponent);

        if (p_addToEngine)
        {
            s_engine.addEntity(l_weapon);
        }

        return l_weapon;
    }

    public static void updateWeapon(String p_name, Entity p_weapon, Entity p_parent)
    {
        WeaponEntry l_weaponEntry = Global.weapons.get(p_name);

        if (l_weaponEntry == null)
        {
            return;
        }

        DirectionComponent l_parentDirectionComponent = Mappers.directionMapper.get(p_parent);
        Direction l_parentDirection = l_parentDirectionComponent.direction;

        EntityStateComponent l_entityStateComponent = Mappers.entityStateMapper.get(p_weapon);
        l_entityStateComponent.enabled = true;

        TransformComponent l_parentTransform = Mappers.transformMapper.get(p_parent);
        Vector3 l_parentPos = l_parentTransform.pos;

        Texture l_texture = Global.assets.get(l_weaponEntry.gfx, Texture.class);
        TextureComponent l_textureComponent = Mappers.textureMapper.get(p_weapon);
        l_textureComponent.texture = l_texture;

        TransformComponent l_transformComponent = Mappers.transformMapper.get(p_weapon);
        l_transformComponent.pos.set(l_parentPos.x + l_weaponEntry.x, l_parentPos.y + l_weaponEntry.y, l_parentPos.z);
        l_transformComponent.origin.set(l_texture.getWidth() / 2.0f, l_texture.getHeight() / 2.0f);
        l_transformComponent.size.set(l_texture.getWidth(), l_texture.getHeight());

        AnchorComponent l_anchorComponent = Mappers.anchorMapper.get(p_weapon);
        if (l_parentDirection == Direction.Right)
        {
            l_anchorComponent.relativePosition.set(l_weaponEntry.x, l_weaponEntry.y, 0);
        }
        else
        {
            l_anchorComponent.relativePosition.set(-l_weaponEntry.x, l_weaponEntry.y, 0);
        }

        Shape l_shape = Polygon.rectangle(
                l_parentPos.x + l_anchorComponent.relativePosition.x,
                l_parentPos.y + l_anchorComponent.relativePosition.y,
                l_texture.getWidth(),
                l_texture.getHeight()
        );
        ColliderComponent l_colliderComponent = new ColliderComponent();
        l_colliderComponent.shape = l_shape;

        DirectionComponent l_directionComponent = Mappers.directionMapper.get(p_weapon);
        l_directionComponent.direction = l_parentDirection;
    }
}
