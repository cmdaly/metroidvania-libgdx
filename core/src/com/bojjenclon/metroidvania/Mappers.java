package com.bojjenclon.metroidvania;

import com.badlogic.ashley.core.ComponentMapper;
import com.bojjenclon.metroidvania.components.*;

/**
 * Created by Cornell on 12/30/2014.
 */
public abstract class Mappers
{
    public static ComponentMapper<AnchorComponent> anchorMapper = ComponentMapper.getFor(AnchorComponent.class);
    public static ComponentMapper<AnimationComponent> animationMapper = ComponentMapper.getFor(AnimationComponent.class);
    public static ComponentMapper<ColliderComponent> colliderMapper = ComponentMapper.getFor(ColliderComponent.class);
    public static ComponentMapper<DashComponent> dashMapper = ComponentMapper.getFor(DashComponent.class);
    public static ComponentMapper<DirectionComponent> directionMapper = ComponentMapper.getFor(DirectionComponent.class);
    public static ComponentMapper<EntityStateComponent> entityStateMapper = ComponentMapper.getFor(EntityStateComponent.class);
    public static ComponentMapper<EquipmentComponent> equipmentMapper = ComponentMapper.getFor(EquipmentComponent.class);
    public static ComponentMapper<HitDelayComponent> hitDelayMapper = ComponentMapper.getFor(HitDelayComponent.class);
    public static ComponentMapper<IdentifierComponent> identifierMapper = ComponentMapper.getFor(IdentifierComponent.class);
    public static ComponentMapper<JumpComponent> jumpMapper = ComponentMapper.getFor(JumpComponent.class);
    public static ComponentMapper<PlayerComponent> playerMapper = ComponentMapper.getFor(PlayerComponent.class);
    public static ComponentMapper<StatsComponent> statsMapper = ComponentMapper.getFor(StatsComponent.class);
    public static ComponentMapper<TextureComponent> textureMapper = ComponentMapper.getFor(TextureComponent.class);
    public static ComponentMapper<TransformComponent> transformMapper = ComponentMapper.getFor(TransformComponent.class);
    public static ComponentMapper<VelocityComponent> velocityMapper = ComponentMapper.getFor(VelocityComponent.class);
    public static ComponentMapper<WeaponComponent> weaponMapper = ComponentMapper.getFor(WeaponComponent.class);
}
