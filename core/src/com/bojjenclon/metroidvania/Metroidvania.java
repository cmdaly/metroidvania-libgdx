package com.bojjenclon.metroidvania;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.bojjenclon.metroidvania.screens.LoadingScreen;

public class Metroidvania extends Game
{
    @Override
    public void create()
    {
        setScreen(new LoadingScreen(this));
    }

    @Override
    public void render()
    {
        GL20 gl = Gdx.gl;
        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        super.render();
    }

    @Override
    public void dispose()
    {
		Global.batch.dispose();
        Global.font.dispose();
        Global.assets.dispose();
    }
}
