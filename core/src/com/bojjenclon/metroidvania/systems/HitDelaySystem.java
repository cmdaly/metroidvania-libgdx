package com.bojjenclon.metroidvania.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.bojjenclon.metroidvania.Mappers;
import com.bojjenclon.metroidvania.components.HitDelayComponent;

/**
 * Created by Cornell on 1/1/2015.
 */
public class HitDelaySystem extends IteratingSystem
{
    public static final float INVULNERABLE_TIME = 1.75f;

    public HitDelaySystem()
    {
        super(Family.all(HitDelayComponent.class).get());
    }

    @Override
    protected void processEntity(Entity p_entity, float p_deltaTime)
    {
        HitDelayComponent l_hitDelayComponent = Mappers.hitDelayMapper.get(p_entity);

        if (l_hitDelayComponent.timer > -1)
        {
            l_hitDelayComponent.timer += p_deltaTime;

            if (l_hitDelayComponent.timer >= INVULNERABLE_TIME)
            {
                l_hitDelayComponent.timer = -1;
                l_hitDelayComponent.canBeHit = true;
            }
        }
    }
}
