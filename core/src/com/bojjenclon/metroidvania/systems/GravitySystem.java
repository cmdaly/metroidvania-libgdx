package com.bojjenclon.metroidvania.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.Vector2;
import com.bojjenclon.metroidvania.Config;
import com.bojjenclon.metroidvania.Mappers;
import com.bojjenclon.metroidvania.components.ColliderComponent;
import com.bojjenclon.metroidvania.components.EntityStateComponent;
import com.bojjenclon.metroidvania.components.VelocityComponent;

/**
 * Created by Cornell on 12/26/2014.
 */
public class GravitySystem extends IteratingSystem
{
    public GravitySystem()
    {
        super(Family.all(VelocityComponent.class, ColliderComponent.class).get());
    }

    @Override
    protected void processEntity(Entity p_entity, float p_deltaTime)
    {
        EntityStateComponent l_entityStateComponent = Mappers.entityStateMapper.get(p_entity);

        if (!l_entityStateComponent.enabled)
        {
            return;
        }

        VelocityComponent l_velocityComponent = Mappers.velocityMapper.get(p_entity);
        Vector2 l_velocity = l_velocityComponent.velocity;

        ColliderComponent l_colliderComponent = Mappers.colliderMapper.get(p_entity);

        if (l_colliderComponent.touchingFloor)
        {
            return;
        }

        if (l_velocity.y > Config.MAX_FALL_SPEED)
        {
            l_velocity.y -= Config.GRAVITY;
        }
    }
}
