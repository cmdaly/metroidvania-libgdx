package com.bojjenclon.metroidvania.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.bojjenclon.metroidvania.Mappers;
import com.bojjenclon.metroidvania.collision.Collision;
import com.bojjenclon.metroidvania.collision.data.CollisionData;
import com.bojjenclon.metroidvania.collision.grid.GridWorld;
import com.bojjenclon.metroidvania.collision.shapes.Shape;
import com.bojjenclon.metroidvania.components.*;
import com.bojjenclon.metroidvania.enums.Identifier;

/**
 * Created by Cornell on 12/26/2014.
 */
public class CollisionSystem extends IteratingSystem
{
    private static final float BOUNCE_BACK_HORIZONTAL = 120.0f;
    private static final float BOUNCE_BACK_VERTICAL = 450.0f;

    private GridWorld m_grid;

    private Array<Entity> m_collisionQueue;
    
    public CollisionSystem(GridWorld p_gridWorld)
    {
        super(Family.getFor(TransformComponent.class, ColliderComponent.class, IdentifierComponent.class));

        m_grid = p_gridWorld;

        m_collisionQueue = new Array<Entity>();
    }

    @Override
    public void update(float p_delta)
    {
        super.update(p_delta);

        for (Entity l_entity : m_collisionQueue)
        {
            TransformComponent l_transformComponent = Mappers.transformMapper.get(l_entity);
            ColliderComponent l_collider = Mappers.colliderMapper.get(l_entity);

            Vector3 l_position = l_transformComponent.pos;
            Shape l_shape = l_collider.shape;

            l_shape.setPosition(l_position.x, l_position.y);

            /*if (l_collider.shape instanceof Circle)
            {
                Circle l_circle = (Circle) l_collider.shape;
                l_circle.center();
            }*/

            checkCollision(l_entity);
        }

        m_collisionQueue.clear();
    }

    @Override
    protected void processEntity(Entity p_entity, float p_deltaTime)
    {
        EntityStateComponent l_entityStateComponent = Mappers.entityStateMapper.get(p_entity);

        if (!l_entityStateComponent.enabled)
        {
            return;
        }

        ColliderComponent l_collider = Mappers.colliderMapper.get(p_entity);

        if (l_collider.isStatic)
        {
            return;
        }

        l_collider.collidedWith.clear();

        m_collisionQueue.add(p_entity);
    }

    private void checkCollision(Entity p_entity)
    {
        TransformComponent l_transformComponent = Mappers.transformMapper.get(p_entity);
        ColliderComponent l_collider = Mappers.colliderMapper.get(p_entity);
        IdentifierComponent l_identifierComponent = Mappers.identifierMapper.get(p_entity);
        DashComponent l_dashComponent = Mappers.dashMapper.get(p_entity);

        Vector3 l_position = l_transformComponent.pos;
        Vector2 l_origin = l_transformComponent.origin;
        Vector2 l_size = l_transformComponent.size;

        Identifier l_identifier = l_identifierComponent.type;

        l_collider.touchingFloor = false;

        Rectangle l_area = new Rectangle(
                l_position.x - l_origin.x, l_position.y - l_origin.y,
                l_size.x, l_size.y
        );

        Array<Entity> l_entities = m_grid.getEntitiesInArea(l_area);

        for (Entity l_otherEntity : l_entities)
        {
            if (p_entity == l_otherEntity)
            {
                continue;
            }

            ColliderComponent l_otherCollider = Mappers.colliderMapper.get(l_otherEntity);

            if (l_otherCollider.collidedWith.contains(p_entity, true))
            {
                continue;
            }
            else if (l_collider.collidedWith.contains(l_otherEntity, true))
            {
                continue;
            }

            AnchorComponent l_anchorComponent = Mappers.anchorMapper.get(p_entity);
            AnchorComponent l_otherAnchorComponent = Mappers.anchorMapper.get(l_otherEntity);

            if (l_anchorComponent != null && l_anchorComponent.parent == l_otherEntity)
            {
                l_collider.collidedWith.add(l_otherEntity);
                l_otherCollider.collidedWith.add(p_entity);

                continue;
            }
            else if (l_otherAnchorComponent != null && l_otherAnchorComponent.parent == p_entity)
            {
                l_collider.collidedWith.add(l_otherEntity);
                l_otherCollider.collidedWith.add(p_entity);

                continue;
            }

            CollisionData l_collisionData = Collision.test(l_collider.shape, l_otherCollider.shape);

            if (l_collisionData != null)
            {
                IdentifierComponent l_otherIdentifierComponent = Mappers.identifierMapper.get(l_otherEntity);
                Identifier l_otherIdentifier = l_otherIdentifierComponent.type;

                Vector3 l_previousPos = l_position.cpy();

                boolean l_updatePosition = true;

                if (l_identifier == Identifier.Weapon)
                {
                    l_updatePosition = weaponHit(p_entity, l_otherEntity);
                }
                else if (l_otherIdentifier == Identifier.Floor)
                {
                    l_updatePosition = floorTouched(l_otherEntity, p_entity);
                }
                else if (l_otherIdentifier == Identifier.Wall)
                {
                    l_updatePosition = wallTouched(l_otherEntity, p_entity);
                }
                else if (l_identifier == Identifier.Player && l_otherIdentifier == Identifier.Enemy)
                {
                    l_updatePosition = playerTouchedEnemy(p_entity, l_otherEntity, l_collisionData);
                }
                else if (l_identifier == Identifier.Enemy && l_otherIdentifier == Identifier.Player)
                {
                    // enemies shouldn't be moved by the player
                    l_updatePosition = false;
                }

                if (l_updatePosition)
                {
                    l_position.x -= l_collisionData.separation.x;
                    l_position.y -= l_collisionData.separation.y;

                    l_collider.shape.setPosition(l_position.x, l_position.y);

                    /*if (l_collider.shape instanceof Circle)
                    {
                        Circle l_circle = (Circle) l_collider.shape;
                        l_circle.center();
                    }*/
                }

                l_collider.collidedWith.add(l_otherEntity);
                if (!l_otherCollider.isStatic)
                {
                    l_otherCollider.collidedWith.add(p_entity);
                }

                m_grid.updateEntity(p_entity, l_area, l_previousPos);
            }
        }
    }

    private boolean weaponHit(Entity p_weapon, Entity p_otherEntity)
    {
        IdentifierComponent l_otherIdentifierComponent = Mappers.identifierMapper.get(p_otherEntity);
        Identifier l_otherIdentifier = l_otherIdentifierComponent.type;

        if (l_otherIdentifier == Identifier.Player || l_otherIdentifier == Identifier.Enemy)
        {
            WeaponComponent l_weaponComponent = Mappers.weaponMapper.get(p_weapon);
            StatsComponent l_otherStatsComponent = Mappers.statsMapper.get(p_otherEntity);
            HitDelayComponent l_otherHitDelayComponent = Mappers.hitDelayMapper.get(p_otherEntity);

            if (l_weaponComponent == null || l_otherStatsComponent == null || l_otherHitDelayComponent == null)
            {
                return false;
            }

            if (l_otherHitDelayComponent.canBeHit)
            {
                l_otherStatsComponent.hp -= l_weaponComponent.damage;

                l_otherHitDelayComponent.canBeHit = false;
                l_otherHitDelayComponent.timer = 0;
            }
        }

        return false;
    }

    private boolean floorTouched(Entity p_floor, Entity p_otherEntity)
    {
        VelocityComponent l_velocityComponent = Mappers.velocityMapper.get(p_otherEntity);

        if (l_velocityComponent != null)
        {
            Vector2 l_velocity = l_velocityComponent.velocity;

            l_velocity.y = 0;
        }

        ColliderComponent l_collider = Mappers.colliderMapper.get(p_otherEntity);
        l_collider.touchingFloor = true;

        JumpComponent l_jumpComponent = Mappers.jumpMapper.get(p_otherEntity);

        if (l_jumpComponent != null)
        {
            l_jumpComponent.jumpDistance = 0;
            l_jumpComponent.canJump = true;
        }

        HitDelayComponent l_hitDelayComponent = Mappers.hitDelayMapper.get(p_otherEntity);

        if (l_hitDelayComponent != null)
        {
            l_hitDelayComponent.landedFromHit = true;
        }

        return true;
    }

    private boolean wallTouched(Entity p_wall, Entity p_otherEntity)
    {
        HitDelayComponent l_hitDelayComponent = Mappers.hitDelayMapper.get(p_otherEntity);

        if (l_hitDelayComponent != null)
        {
            l_hitDelayComponent.landedFromHit = true;
        }

        return true;
    }

    private boolean playerTouchedEnemy(Entity p_player, Entity p_enemy, CollisionData p_collisionData)
    {
        DashComponent l_dashComponent = Mappers.dashMapper.get(p_player);
        HitDelayComponent l_hitDelayComponent = Mappers.hitDelayMapper.get(p_player);

        if (l_dashComponent != null && (l_dashComponent.isDashing || l_dashComponent.justDashed))
        {
            return l_dashComponent.justDashed;
        }
        else if (l_hitDelayComponent.canBeHit)
        {
            StatsComponent l_statsComponent = Mappers.statsMapper.get(p_player);
            l_statsComponent.hp--;

            VelocityComponent l_velocityComponent = Mappers.velocityMapper.get(p_player);
            TransformComponent l_transformComponent = Mappers.transformMapper.get(p_player);
            Vector3 l_position = l_transformComponent.pos;

            l_hitDelayComponent.canBeHit = false;
            l_hitDelayComponent.timer = 0;
            l_hitDelayComponent.landedFromHit = false;

            if (p_collisionData.separation.x >= 0)
            {
                l_position.x--;
                l_velocityComponent.velocity.x = -BOUNCE_BACK_HORIZONTAL;
            }
            else
            {
                l_position.x++;
                l_velocityComponent.velocity.x = BOUNCE_BACK_HORIZONTAL;
            }

            l_position.y++;
            l_velocityComponent.velocity.y = BOUNCE_BACK_VERTICAL;
        }

        return true;
    }
}
