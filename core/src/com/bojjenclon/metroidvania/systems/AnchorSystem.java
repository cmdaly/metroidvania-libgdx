package com.bojjenclon.metroidvania.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.Vector3;
import com.bojjenclon.metroidvania.Mappers;
import com.bojjenclon.metroidvania.components.AnchorComponent;
import com.bojjenclon.metroidvania.components.EntityStateComponent;
import com.bojjenclon.metroidvania.components.TransformComponent;

/**
 * Created by Cornell on 12/30/2014.
 */
public class AnchorSystem extends IteratingSystem
{
    public AnchorSystem()
    {
        super(Family.getFor(TransformComponent.class, AnchorComponent.class));
    }

    @Override
    protected void processEntity(Entity p_entity, float p_deltaTime)
    {
        EntityStateComponent l_entityStateComponent = Mappers.entityStateMapper.get(p_entity);

        if (!l_entityStateComponent.enabled)
        {
            return;
        }

        TransformComponent l_transformComponent = Mappers.transformMapper.get(p_entity);
        Vector3 l_position = l_transformComponent.pos;

        AnchorComponent l_anchorComponent = Mappers.anchorMapper.get(p_entity);
        Vector3 l_relativePosition = l_anchorComponent.relativePosition;

        TransformComponent l_parentTransformComponent = Mappers.transformMapper.get(l_anchorComponent.parent);
        Vector3 l_parentPosition = l_parentTransformComponent.pos;

        l_position.set(
                l_parentPosition.x + l_relativePosition.x,
                l_parentPosition.y + l_relativePosition.y,
                l_parentPosition.z + l_relativePosition.z
        );
    }
}
