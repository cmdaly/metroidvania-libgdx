package com.bojjenclon.metroidvania.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.bojjenclon.metroidvania.AnimatedImage;
import com.bojjenclon.metroidvania.Mappers;
import com.bojjenclon.metroidvania.components.AnimationComponent;
import com.bojjenclon.metroidvania.components.EntityStateComponent;
import com.bojjenclon.metroidvania.components.EquipmentComponent;

/**
 * Created by Cornell on 1/1/2015.
 */
public class WeaponSystem extends IteratingSystem
{
    public WeaponSystem()
    {
        super(Family.all(EquipmentComponent.class, AnimationComponent.class).get());
    }

    @Override
    protected void processEntity(Entity p_entity, float p_deltaTime)
    {
        EntityStateComponent l_entityStateComponent = Mappers.entityStateMapper.get(p_entity);

        if (!l_entityStateComponent.enabled)
        {
            return;
        }

        AnimationComponent l_animationComponent = Mappers.animationMapper.get(p_entity);
        AnimatedImage l_animatedImage = l_animationComponent.animatedImage;

        EquipmentComponent l_equipmentComponent = Mappers.equipmentMapper.get(p_entity);

        if (l_equipmentComponent.weaponEntity == null)
        {
            return;
        }

        EntityStateComponent l_weaponState = Mappers.entityStateMapper.get(l_equipmentComponent.weaponEntity);

        if (l_weaponState.enabled && !l_animatedImage.getAnimationName().equals("attack"))
        {
            l_weaponState.enabled = false;
        }
    }
}
