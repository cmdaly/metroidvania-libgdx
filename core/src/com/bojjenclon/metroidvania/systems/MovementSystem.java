package com.bojjenclon.metroidvania.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.bojjenclon.metroidvania.Mappers;
import com.bojjenclon.metroidvania.components.EntityStateComponent;
import com.bojjenclon.metroidvania.components.TransformComponent;
import com.bojjenclon.metroidvania.components.VelocityComponent;

/**
 * Created by Cornell on 12/26/2014.
 */
public class MovementSystem extends IteratingSystem
{
    public MovementSystem()
    {
        super(Family.all(TransformComponent.class, VelocityComponent.class).get());
    }

    @Override
    protected void processEntity(Entity p_entity, float p_deltaTime)
    {
        EntityStateComponent l_entityStateComponent = Mappers.entityStateMapper.get(p_entity);

        if (!l_entityStateComponent.enabled)
        {
            return;
        }

        TransformComponent l_transformComponent = Mappers.transformMapper.get(p_entity);
        Vector3 l_position = l_transformComponent.pos;

        VelocityComponent l_velocityComponent = Mappers.velocityMapper.get(p_entity);
        Vector2 l_oldVelocity = l_velocityComponent.oldVelocity;
        Vector2 l_velocity = l_velocityComponent.velocity;

        Vector2 l_averagedVelocity = new Vector2(
                Math.round(0.5 * (l_oldVelocity.x + l_velocity.x) * p_deltaTime),
                Math.round(0.5 * (l_oldVelocity.y + l_velocity.y) * p_deltaTime)
        );

        l_position.x += l_averagedVelocity.x;
        l_position.y += l_averagedVelocity.y;
    }
}
