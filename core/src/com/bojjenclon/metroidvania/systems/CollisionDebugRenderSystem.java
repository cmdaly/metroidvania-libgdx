package com.bojjenclon.metroidvania.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.Array;
import com.bojjenclon.metroidvania.Config;
import com.bojjenclon.metroidvania.Mappers;
import com.bojjenclon.metroidvania.collision.shapes.Circle;
import com.bojjenclon.metroidvania.collision.shapes.Shape;
import com.bojjenclon.metroidvania.components.ColliderComponent;
import com.bojjenclon.metroidvania.components.EntityStateComponent;

/**
 * Created by Cornell on 1/1/2015.
 */
public class CollisionDebugRenderSystem extends IteratingSystem
{
    private ShapeRenderer m_renderer;
    private OrthographicCamera m_camera;

    private Array<Entity> m_renderQueue;

    public CollisionDebugRenderSystem()
    {
        super(Family.all(ColliderComponent.class).get());

        m_renderQueue = new Array<Entity>();

        m_renderer = new ShapeRenderer();

        m_camera = new OrthographicCamera(Config.WIDTH, Config.HEIGHT);
        m_camera.position.set(Config.WIDTH / 2, Config.HEIGHT / 2, 0);
    }

    @Override
    public void update(float p_delta)
    {
        super.update(p_delta);

        m_camera.update();
        m_renderer.setProjectionMatrix(m_camera.combined);

        m_renderer.begin(ShapeRenderer.ShapeType.Line);
        m_renderer.setColor(1, 0, 0, 1);

        for (Entity l_entity : m_renderQueue)
        {
            EntityStateComponent l_entityStateComponent = Mappers.entityStateMapper.get(l_entity);

            if (!l_entityStateComponent.enabled)
            {
                continue;
            }

            ColliderComponent l_colliderComponent = Mappers.colliderMapper.get(l_entity);
            Shape l_shape = l_colliderComponent.shape;

            if (l_shape instanceof Circle)
            {
                Circle l_circle = (Circle) l_shape;
                m_renderer.circle(l_circle.getX() + (l_circle.getRadius() / 2), l_circle.getY() + (l_circle.getRadius() / 2), l_circle.getRadius());
            }
            else
            {
                m_renderer.rect(l_shape.getX() - (l_shape.getWidth() / 2), l_shape.getY() - (l_shape.getHeight() / 2), l_shape.getWidth(), l_shape.getHeight());
            }
        }

        m_renderer.end();

        m_renderQueue.clear();
    }

    @Override
    protected void processEntity(Entity p_entity, float p_deltaTime)
    {
        m_renderQueue.add(p_entity);
    }
}
