package com.bojjenclon.metroidvania.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.bojjenclon.metroidvania.AnimatedImage;
import com.bojjenclon.metroidvania.Mappers;
import com.bojjenclon.metroidvania.components.AnimationComponent;
import com.bojjenclon.metroidvania.components.EntityStateComponent;

/**
 * Created by Cornell on 1/1/2015.
 */
public class AnimationSystem extends IteratingSystem
{
    public AnimationSystem()
    {
        super(Family.all(AnimationComponent.class).get());
    }

    @Override
    protected void processEntity(Entity p_entity, float p_deltaTime)
    {
        EntityStateComponent l_entityStateComponent = Mappers.entityStateMapper.get(p_entity);

        if (!l_entityStateComponent.enabled)
        {
            return;
        }

        AnimationComponent l_animationComponent = Mappers.animationMapper.get(p_entity);
        AnimatedImage l_animatedImage = l_animationComponent.animatedImage;

        l_animatedImage.update(p_deltaTime);

        if (l_animatedImage.getAnimationName().equals("attack") && l_animatedImage.isFinished())
        {
            l_animatedImage.play("idle");
        }
    }
}
