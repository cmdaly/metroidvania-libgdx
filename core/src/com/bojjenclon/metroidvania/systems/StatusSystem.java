package com.bojjenclon.metroidvania.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.bojjenclon.metroidvania.Mappers;
import com.bojjenclon.metroidvania.components.EntityStateComponent;
import com.bojjenclon.metroidvania.components.StatsComponent;

/**
 * Created by Cornell on 1/1/2015.
 */
public class StatusSystem extends IteratingSystem
{
    public StatusSystem()
    {
        super(Family.all(StatsComponent.class).get());
    }

    @Override
    protected void processEntity(Entity p_entity, float p_deltaTime)
    {
        EntityStateComponent l_entityStateComponent = Mappers.entityStateMapper.get(p_entity);

        if (!l_entityStateComponent.enabled)
        {
            return;
        }

        StatsComponent l_statsComponent = Mappers.statsMapper.get(p_entity);

        if (l_statsComponent.hp <= 0)
        {
            l_entityStateComponent.enabled = false;
        }
    }
}
