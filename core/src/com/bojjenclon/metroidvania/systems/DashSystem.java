package com.bojjenclon.metroidvania.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.bojjenclon.metroidvania.Mappers;
import com.bojjenclon.metroidvania.components.DashComponent;
import com.bojjenclon.metroidvania.components.DirectionComponent;
import com.bojjenclon.metroidvania.components.EntityStateComponent;
import com.bojjenclon.metroidvania.components.VelocityComponent;
import com.bojjenclon.metroidvania.enums.Direction;

/**
 * Created by Cornell on 1/2/2015.
 */
public class DashSystem extends IteratingSystem
{
    public static final float DASH_END_DISTANCE = 3700.0f;
    public static final float DASH_SPEED = 370.0f;
    public static final float DASH_DELAY_TIME = 0.5f;

    public DashSystem()
    {
        super(Family.all(DashComponent.class, VelocityComponent.class).get());
    }

    @Override
    protected void processEntity(Entity p_entity, float p_deltaTime)
    {
        EntityStateComponent l_entityStateComponent = Mappers.entityStateMapper.get(p_entity);

        if (!l_entityStateComponent.enabled)
        {
            return;
        }

        DashComponent l_dashComponent = Mappers.dashMapper.get(p_entity);

        if (l_dashComponent.justDashed)
        {
            l_dashComponent.justDashed = false;
        }

        if (!l_dashComponent.isDashing)
        {
            if (l_dashComponent.dashDelay > -1)
            {
                l_dashComponent.dashDelay += p_deltaTime;

                if (l_dashComponent.dashDelay >= DASH_DELAY_TIME)
                {
                    l_dashComponent.dashDelay = -1;
                    l_dashComponent.canDash = true;
                }
            }

            return;
        }

        if (l_dashComponent.dashDistance < DASH_END_DISTANCE)
        {
            DirectionComponent l_directionComponent = Mappers.directionMapper.get(p_entity);
            VelocityComponent l_velocityComponent = Mappers.velocityMapper.get(p_entity);

            l_velocityComponent.velocity.x = (l_directionComponent.direction == Direction.Right ? DASH_SPEED : -DASH_SPEED);
            l_dashComponent.dashDistance += DASH_SPEED;
        }
        else
        {
            l_dashComponent.dashDistance = 0;
            l_dashComponent.isDashing = false;
            l_dashComponent.dashDelay = 0;
            l_dashComponent.justDashed = true;
        }
    }
}
