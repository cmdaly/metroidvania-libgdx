package com.bojjenclon.metroidvania.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.bojjenclon.metroidvania.Mappers;
import com.bojjenclon.metroidvania.components.PlayerComponent;
import com.bojjenclon.metroidvania.components.StatsComponent;
import com.bojjenclon.metroidvania.ui.HUD;

/**
 * Created by Cornell on 1/7/2015.
 */
public class HUDSystem extends IteratingSystem
{
    private final HUD m_hud;

    public HUDSystem(final HUD p_hud)
    {
        super(Family.all(PlayerComponent.class, StatsComponent.class).get());

        m_hud = p_hud;
    }

    @Override
    protected void processEntity(Entity p_entity, float p_deltaTime)
    {
        StatsComponent l_statsComponent = Mappers.statsMapper.get(p_entity);

        m_hud.updateHealthBar(l_statsComponent.hp, l_statsComponent.maxHP);
    }
}
