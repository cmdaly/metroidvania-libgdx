package com.bojjenclon.metroidvania.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.bojjenclon.metroidvania.Mappers;
import com.bojjenclon.metroidvania.components.EntityStateComponent;
import com.bojjenclon.metroidvania.components.VelocityComponent;

/**
 * Created by Cornell on 12/26/2014.
 */
public class VelocitySmoothingSystem extends IteratingSystem
{
    public VelocitySmoothingSystem()
    {
        super(Family.all(VelocityComponent.class).get());
    }

    @Override
    protected void processEntity(Entity p_entity, float p_delta)
    {
        EntityStateComponent l_entityStateComponent = Mappers.entityStateMapper.get(p_entity);

        if (!l_entityStateComponent.enabled)
        {
            return;
        }

        VelocityComponent l_velocityComponent = Mappers.velocityMapper.get(p_entity);
        l_velocityComponent.oldVelocity.set(l_velocityComponent.velocity);
    }
}
