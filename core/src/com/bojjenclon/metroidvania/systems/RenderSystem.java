package com.bojjenclon.metroidvania.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.bojjenclon.metroidvania.AnimatedImage;
import com.bojjenclon.metroidvania.Config;
import com.bojjenclon.metroidvania.Mappers;
import com.bojjenclon.metroidvania.components.*;
import com.bojjenclon.metroidvania.enums.Direction;

import java.util.Comparator;

/**
 * Created by Cornell on 12/24/2014.
 */
public class RenderSystem extends IteratingSystem
{
    private SpriteBatch m_batch;
    private OrthographicCamera m_camera;

    private Array<Entity> m_renderQueue;
    private Comparator<Entity> m_comparator;

    public RenderSystem(SpriteBatch p_batch)
    {
        super(Family.all(EntityStateComponent.class, TransformComponent.class).one(TextureComponent.class, AnimationComponent.class).get());

        m_renderQueue = new Array<Entity>();

        m_comparator = new Comparator<Entity>()
        {
            @Override
            public int compare(Entity p_lhs, Entity p_rhs)
            {
                TransformComponent l_lhsTransform = Mappers.transformMapper.get(p_lhs);
                TransformComponent l_rhsTransform = Mappers.transformMapper.get(p_rhs);

                return (int) Math.signum(l_lhsTransform.pos.z - l_rhsTransform.pos.z);
            }
        };

        m_batch = p_batch;

        m_camera = new OrthographicCamera(Config.WIDTH, Config.HEIGHT);
        m_camera.position.set(Config.WIDTH / 2, Config.HEIGHT / 2, 0);
    }

    @Override
    public void update(float p_delta)
    {
        super.update(p_delta);

        m_renderQueue.sort(m_comparator);

        m_camera.update();
        m_batch.setProjectionMatrix(m_camera.combined);

        m_batch.begin();

        for (Entity l_entity : m_renderQueue)
        {
            EntityStateComponent l_entityStateComponent = Mappers.entityStateMapper.get(l_entity);

            if (!l_entityStateComponent.enabled)
            {
                continue;
            }

            boolean l_isHit = false;

            HitDelayComponent l_hitDelayComponent = Mappers.hitDelayMapper.get(l_entity);

            if (l_hitDelayComponent != null)
            {
                l_isHit = !l_hitDelayComponent.canBeHit;
            }

            DashComponent l_dashComponent = Mappers.dashMapper.get(l_entity);

            TransformComponent l_transformComponent = Mappers.transformMapper.get(l_entity);
            Vector3 l_position = l_transformComponent.pos;
            Vector2 l_scale = l_transformComponent.scale;
            float l_rotation = l_transformComponent.rotation;

            TextureComponent l_textureComponent = Mappers.textureMapper.get(l_entity);

            Color l_color = Color.WHITE;

            if (l_isHit)
            {
                l_color = Color.RED;
            }
            else if (l_dashComponent != null && l_dashComponent.isDashing)
            {
                l_color = Color.GREEN;
            }

            boolean l_flip = false;

            DirectionComponent l_directionComponent = Mappers.directionMapper.get(l_entity);

            if (l_directionComponent != null)
            {
                l_flip = (l_directionComponent.direction == Direction.Left);
            }

            if (l_textureComponent != null)
            {
                renderTexture(l_textureComponent.texture, l_position, l_scale, l_rotation, l_color, l_flip);
            }

            AnimationComponent l_animationComponent = Mappers.animationMapper.get(l_entity);

            if (l_animationComponent != null)
            {
                renderAnimatedImage(l_animationComponent.animatedImage, l_position, l_scale, l_rotation, l_color, l_flip);
            }
        }

        m_batch.end();

        m_renderQueue.clear();
    }

    @Override
    protected void processEntity(Entity p_entity, float p_deltaTime)
    {
        m_renderQueue.add(p_entity);
    }

    private void renderTexture(Texture p_texture, Vector3 p_position, Vector2 p_scale, float p_rotation, Color p_color, boolean p_flip)
    {
        if (p_texture == null)
        {
            return;
        }

        float l_width = p_texture.getWidth();
        float l_height = p_texture.getHeight();
        float l_originX = (l_width / 2.0f);
        float l_originY = (l_height / 2.0f);

        m_batch.setColor(p_color);

        m_batch.draw(
                p_texture,
                p_position.x - l_originX, p_position.y - l_originY,
                l_originX, l_originY,
                l_width, l_height,
                p_scale.x, p_scale.y,
                p_rotation,
                // Texture only, remove the rest when using TextureRegion
                0, 0, // source position
                (int) l_width, (int) l_height, // source size
                p_flip, false // flip x/y
        );

        m_batch.setColor(Color.WHITE);
    }

    private void renderAnimatedImage(AnimatedImage p_animatedImage, Vector3 p_position, Vector2 p_scale, float p_rotation, Color p_color, boolean p_flip)
    {
        if (p_animatedImage == null)
        {
            return;
        }

        TextureRegion l_textureRegion = p_animatedImage.getCurFrame();

        float l_width = l_textureRegion.getRegionWidth();
        float l_height = l_textureRegion.getRegionHeight();
        float l_originX = (l_width / 2.0f);
        float l_originY = (l_height / 2.0f);

        if (p_flip && !l_textureRegion.isFlipX())
        {
            l_textureRegion.flip(true, false);
        }
        else if (!p_flip && l_textureRegion.isFlipX())
        {
            l_textureRegion.flip(true, false);
        }

        m_batch.setColor(p_color);

        m_batch.draw(
                l_textureRegion,
                p_position.x - l_originX, p_position.y - l_originY,
                l_originX, l_originY,
                l_width, l_height,
                p_scale.x, p_scale.y,
                p_rotation
        );

        m_batch.setColor(Color.WHITE);
    }
}
