package com.bojjenclon.metroidvania.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.bojjenclon.metroidvania.*;
import com.bojjenclon.metroidvania.components.*;
import com.bojjenclon.metroidvania.enums.Direction;

/**
 * Created by Cornell on 12/25/2014.
 */
public class InputSystem extends IteratingSystem
{
    private static final float MOVE_SPEED = 160.0f;
    private static final float JUMP_MOVE_MULTIPLIER = 1.4f;

    private static final float JUMP_FORCE = 180.0f;
    private static final float MAX_AIR_TIME = 3850.0f;

    private final Metroidvania m_game;

    public InputSystem(final Metroidvania p_game)
    {
        super(Family.all(PlayerComponent.class).get());

        m_game = p_game;
    }

    @Override
    protected void processEntity(Entity p_entity, float p_deltaTime)
    {
        EntityStateComponent l_entityStateComponent = Mappers.entityStateMapper.get(p_entity);

        if (!l_entityStateComponent.enabled)
        {
            return;
        }

        DashComponent l_dashComponent = Mappers.dashMapper.get(p_entity);

        if (l_dashComponent.isDashing)
        {
            return;
        }

        HitDelayComponent l_hitDelayComponent = Mappers.hitDelayMapper.get(p_entity);

        if (!l_hitDelayComponent.landedFromHit)
        {
            return;
        }

        // keyboard input
        boolean l_leftPressed = Gdx.input.isKeyPressed(Input.Keys.A) || Gdx.input.isKeyPressed(Input.Keys.LEFT);
        boolean l_rightPressed = Gdx.input.isKeyPressed(Input.Keys.D) || Gdx.input.isKeyPressed(Input.Keys.RIGHT);
        boolean l_jumpPressed = Gdx.input.isKeyPressed(Input.Keys.W) || Gdx.input.isKeyPressed(Input.Keys.UP);
        boolean l_attackPressed = Gdx.input.isKeyJustPressed(Input.Keys.F);
        boolean l_dashLeftPressed = Gdx.input.isKeyJustPressed(Input.Keys.Q);
        boolean l_dashRightPressed = Gdx.input.isKeyJustPressed(Input.Keys.E);

        // controller input
        Array<Controller> l_controllerArray = Controllers.getControllers();
        Controller l_controller = (l_controllerArray.size > 0 ? l_controllerArray.get(0) : null);

        if (l_controller != null)
        {
            l_leftPressed = l_leftPressed || (l_controller.getAxis(XBox360Pad.AXIS_LEFT_X) < -Config.leftAxisDeadZone);
            l_rightPressed = l_rightPressed || (l_controller.getAxis(XBox360Pad.AXIS_LEFT_X) > Config.leftAxisDeadZone);
            l_jumpPressed = l_jumpPressed || l_controller.getButton(XBox360Pad.BUTTON_A);
            l_attackPressed = l_attackPressed || l_controller.getButton(XBox360Pad.BUTTON_X);
            l_dashLeftPressed = l_dashLeftPressed || l_controller.getAxis(XBox360Pad.AXIS_LEFT_TRIGGER) > Config.leftTriggerDeadZone;
            l_dashRightPressed = l_dashRightPressed || l_controller.getAxis(XBox360Pad.AXIS_RIGHT_TRIGGER) < -Config.rightTriggerDeadZone;
        }

        AnimationComponent l_animationComponent = Mappers.animationMapper.get(p_entity);
        AnimatedImage l_animatedImage = l_animationComponent.animatedImage;

        boolean l_isAttacking = l_animatedImage.getAnimationName().equals("attack");

        DirectionComponent l_directionComponent = Mappers.directionMapper.get(p_entity);

        VelocityComponent l_velocityComponent = Mappers.velocityMapper.get(p_entity);
        Vector2 l_velocity = l_velocityComponent.velocity;

        if ((l_dashLeftPressed || l_dashRightPressed) && l_dashComponent.canDash && !l_isAttacking)
        {
            l_dashComponent.dashDistance = 0;
            l_dashComponent.isDashing = true;
            l_dashComponent.canDash = false;

            if (l_dashLeftPressed && l_directionComponent.direction != Direction.Left)
            {
                l_directionComponent.direction = Direction.Left;
            }
            else if (l_dashRightPressed && l_directionComponent.direction != Direction.Right)
            {
                l_directionComponent.direction = Direction.Right;
            }

            return;
        }

        JumpComponent l_jumpComponent = Mappers.jumpMapper.get(p_entity);

        if (l_jumpPressed)
        {
            if (l_jumpComponent.jumpDistance < MAX_AIR_TIME && l_jumpComponent.canJump)
            {
                l_velocity.y += JUMP_FORCE;
                if (l_velocity.y > Config.MAX_JUMP_SPEED)
                {
                    l_velocity.y = Config.MAX_JUMP_SPEED;
                }

                l_jumpComponent.jumpDistance += l_velocity.y;

                if (l_jumpComponent.jumpDistance >= MAX_AIR_TIME)
                {
                    l_jumpComponent.canJump = false;
                }
            }
        }

        if (l_leftPressed)
        {
            l_velocity.x = -MOVE_SPEED;

            if (l_jumpComponent.jumpDistance > 0)
            {
                l_velocity.x *= JUMP_MOVE_MULTIPLIER;
            }

            if (!l_isAttacking)
            {
                l_directionComponent.direction = Direction.Left;
            }
        }
        else if (l_rightPressed)
        {
            l_velocity.x = MOVE_SPEED;;

            if (l_jumpComponent.jumpDistance > 0)
            {
                l_velocity.x *= JUMP_MOVE_MULTIPLIER;
            }

            if (!l_isAttacking)
            {
                l_directionComponent.direction = Direction.Right;
            }
        }
        else
        {
            l_velocity.x = 0;
        }

        if (l_attackPressed)
        {
            if (!l_isAttacking)
            {
                EntityManager.createWeapon("Long Sword", p_entity, true);

                l_animatedImage.play("attack");
            }
        }
    }
}
